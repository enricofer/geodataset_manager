# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GDM - class GDM_albero
                                 A QGIS plugin
 compilazione assistita metadati regione veneto
                              -------------------
        begin                : 2014-12-04
        git sha              : $Format:%H$
        copyright            : (C) 2014 by Enrico Ferreguti
        email                : enricofer@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

from qgis.core import QgsCoordinateReferenceSystem, QgsCoordinateTransform, QgsPoint, QgsExpressionContextUtils
from qgis.gui import QgsMessageBar

class gestore_estensioni:
    def __init__(self,parent_class):
        self.iface = parent_class.iface
        self.nord  = parent_class.dock.ext_nord
        self.sud   = parent_class.dock.ext_sud
        self.est   = parent_class.dock.ext_est
        self.ovest = parent_class.dock.ext_ovest
        self.settaggi = parent_class.gestioneSettaggi
        self.dock = parent_class.dock
        parent_class.dock.ext_layer_button.clicked.connect(self.getCurrentLayerExtent)
        parent_class.dock.ext_canvas_button.clicked.connect(self.getCurrentCanvasExtent)
        parent_class.dock.ext_restore_default.clicked.connect(self.restoreDefaultExtent)
        parent_class.dock.ext_save_default.clicked.connect(self.saveDefaultExtent)
        self.restoreDefaultExtent()


    def transformToWGS84(self, pPoint):
        # transformation from the current SRS to WGS84
        crsSrc = self.iface.mapCanvas().mapRenderer().destinationCrs() # get current crs
        crsDest = QgsCoordinateReferenceSystem(4326)  # WGS 84
        xform = QgsCoordinateTransform(crsSrc, crsDest)
        return xform.transform(pPoint) # forward transformation: src -> dest


    def getCurrentLayerExtent(self):
        if self.iface.activeLayer():
            print self.iface.activeLayer().name()
            extent = self.iface.activeLayer().extent()
            self.setWidgetsFromExtent(extent)

    def getCurrentCanvasExtent(self):
        if self.iface.activeLayer():
            extent = self.iface.mapCanvas().extent()
            self.setWidgetsFromExtent(extent)

    def setWidgetsFromExtent(self,extent,precision=5):
        pointTopRightWgs84 = self.transformToWGS84(QgsPoint(extent.xMaximum(),extent.yMaximum()))
        pointBottomLeftWgs84 = self.transformToWGS84(QgsPoint(extent.xMinimum(),extent.yMinimum()))
        self.nord.setText("{0:.6f}".format(pointTopRightWgs84.y()))
        self.est.setText("{0:.6f}".format(pointTopRightWgs84.x()))
        self.sud.setText("{0:.6f}".format(pointBottomLeftWgs84.y()))
        self.ovest.setText("{0:.6f}".format(pointBottomLeftWgs84.x()))

    def saveDefaultExtent(self):
        content = self.nord.text()+":"+self.est.text()+":"+self.sud.text()+":"+self.ovest.text()
        self.settaggi.setEstensioni(content)
        #QgsExpressionContextUtils.setGlobalVariable("idt_estensioni", content)

    def restoreDefaultExtent(self):
        #content = QgsExpressionContextUtils.globalScope().variable("idt_estensioni")
        content = self.settaggi.estensioni
        if content:
            content_pack = content.split(':')
            self.nord.setText(content_pack[0])
            self.est.setText(content_pack[1])
            self.sud.setText(content_pack[2])
            self.ovest.setText(content_pack[3])
        else:
            self.iface.messageBar().pushMessage("Attenzione", "Definire estensioni predefinite",
                                                level=QgsMessageBar.WARNING)

    def corrente(self):
        if self.nord.text():
            return self.ovest.text(), self.nord.text(), self.est.text(), self.sud.text()
        else:
            self.iface.messageBar().pushMessage("Attenzione", "Definire estensioni predefinite", level=QgsMessageBar.WARNING)
            self.dock.tabWidget.setCurrentIndex(0)
            return "","","",""

