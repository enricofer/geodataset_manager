# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GDM - class GDM_albero
                                 A QGIS plugin
 compilazione assistita metadati regione veneto
                              -------------------
        begin                : 2014-12-04
        git sha              : $Format:%H$
        copyright            : (C) 2014 by Enrico Ferreguti
        email                : enricofer@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

#from PyQt4 import QtCore, QtGui

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4 import  uic
from qgis.core import *
#from ui_associa_dialog import Ui_associaDialog

import os
import shutil
# create the dialog for zoom to point
Ui_associaDialog, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'ui_associa_dialog.ui'))

class associaDialog(QDialog, Ui_associaDialog):
    def __init__(self,parent):
        QDialog.__init__(self)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.mapLayerRegistry = QgsMapLayerRegistry.instance()
        self.iface = parent.iface
        self.parent = parent
        self.setupUi(self)
        self.buttonBox.accepted.connect(self.associa)
        self.buttonBox.rejected.connect(self.cancel)
        self.layerGeom = ""
        self.codTema = ""
        self.enabledisableClassiAggiunte(False)
        self.classeAggiuntaCheck.stateChanged.connect(self.classeAggiuntaCheckChangedAction)
        self.codiceClasseEdit.textChanged.connect(self.nomeFileUpdate)
        self.descrizioneClasseEdit.textChanged.connect(self.nomeFileUpdate)
        self.classiPrevisteCombo.currentIndexChanged.connect(self.nomeFileUpdate)

    def resetDialog(self):
        self.classeAggiuntaCheck.stateChanged.disconnect(self.classeAggiuntaCheckChangedAction)
        self.codiceClasseEdit.textChanged.disconnect(self.nomeFileUpdate)
        self.descrizioneClasseEdit.textChanged.disconnect(self.nomeFileUpdate)
        self.classiPrevisteCombo.currentIndexChanged.disconnect(self.nomeFileUpdate)
        self.codiceTemaEdit.setText('')
        self.codiceGeomEdit.setText('')
        self.descrizioneClasseEdit.setText('')
        self.codiceClasseEdit.setText('')
        self.classeAggiuntaCheck.setCheckState(False)
        self.nomeLabel.setText('')
        self.classiPrevisteCombo.setEnabled(True)
        self.classiPrevisteCombo.clear()
        self.classeAggiuntaCheck.stateChanged.connect(self.classeAggiuntaCheckChangedAction)
        self.codiceClasseEdit.textChanged.connect(self.nomeFileUpdate)
        self.descrizioneClasseEdit.textChanged.connect(self.nomeFileUpdate)
        self.classiPrevisteCombo.currentIndexChanged.connect(self.nomeFileUpdate)



    def enabledisableClassiAggiunte(self,bool):
        self.codiceTemaEdit.setEnabled(False)
        self.codiceTemaEdit.setText(self.codTema)
        self.codiceTemaLabel.setEnabled(bool)
        self.codiceClasseEdit.setEnabled(bool)
        self.codiceClasseLabel.setEnabled(bool)
        self.codiceGeomEdit.setEnabled(False)
        if self.layerGeom:
            self.codiceGeomEdit.setText(str(int(self.layerGeom)+5))
        else:
            self.codiceGeomEdit.setText("")
        self.codiceGeomLabel.setEnabled(bool)
        self.descrizioneClasseEdit.setEnabled(bool)
        self.descrizioneClasseLabel.setEnabled(bool)

    def classeAggiuntaCheckChangedAction(self):
        if self.classeAggiuntaCheck.checkState() == Qt.Unchecked:
            #self.enabledisableClassiAggiunte(False)
            self.buttonBox.button( QDialogButtonBox.Ok).setEnabled(False)
            self.classiPrevisteCombo.setEnabled(True)
            self.codiceClasseEdit.setEnabled(False)
            self.descrizioneClasseEdit.setEnabled(False)
        else:
            #self.enabledisableClassiAggiunte(True)
            self.buttonBox.button( QDialogButtonBox.Ok).setEnabled(True)
            self.classiPrevisteCombo.setEnabled(False)
            self.codiceClasseEdit.setEnabled(True)
            self.descrizioneClasseEdit.setEnabled(True)

    def verificaNome(self):
        nameValidator = QRegExp("^[abc][0-9]{7}[_][A-Za-z0-9]{1,17}$")
        if nameValidator.exactMatch(self.nomeLabel.text()):
            self.nomeLabel.setStyleSheet("QLabel { color : green; }")
            self.buttonBox.button( QDialogButtonBox.Ok).setEnabled(True)
            return True
        else:
            self.nomeLabel.setStyleSheet("QLabel { color : red; }")
            self.buttonBox.button( QDialogButtonBox.Ok).setEnabled(False)
            return None


    def nomeFileUpdate(self, classeCombo=None):
        if self.classeAggiuntaCheck.isChecked():
            nomeFile = self.codiceTemaEdit.text()+self.codiceClasseEdit.text()+self.codiceGeomEdit.text()+"_"+self.descrizioneClasseEdit.text()
            self.nomeLabel.setText(nomeFile)
        else:
            self.nomeLabel.setText(self.classiPrevisteCombo.currentText())
        self.verificaNome()


    def cancel(self):
        #self.hide()
        pass

    def populateComboBox(self,combo,list,predef = None,sort = None):
        #procedure to fill specified combobox with provided list
        combo.clear()
        model=QStandardItemModel(combo)
        for elem in list:
            if elem:
                item = QStandardItem(elem)
                model.appendRow(item)
        if sort:
            model.sort(0)
        combo.setModel(model)
        if predef:
            combo.insertItem(0,predef)
            combo.setCurrentIndex(0)

    def listaLayers(self):
        ll = self.mapLayerRegistry.mapLayers()
        print self.mapLayerRegistry.mapLayers()
        for layer in self.mapLayerRegistry.mapLayers():
            if layer.type() == QgsMapLayer.VectorLayer:
                ll[layer.name()]=layer.id()
        return ll


    def filtraClassi(self,elemento):
        return elemento[:5] == self.codTema and elemento[7] == self.layerGeom


    def associaFileATema(self,temaDirPath):
        self.temaDirPath = temaDirPath
        self.temaDir = QFileInfo(temaDirPath)
        self.codTema = self.temaDir.completeBaseName()[:5]
        self.sintesiClassi = self.parent.metadataTool.sintesiClassiR.keys()
        self.filesPath = QFileDialog.getOpenFileNamesAndFilter(None,"selezionare i files da aggiungere alla classe",self.parent.settaggi.path_classi,'*.*')
        self.cLayer = None
        if self.filesPath:
            if len(self.filesPath) == 1:
                self.layerGeom = "0"
                self.associaFunc = self.associaFILE
            else:
                self.layerGeom = "4"
                self.associaFunc = self.associaDATASET

            self.classiXTema = filter(self.filtraClassi,self.sintesiClassi)

            if self.classiXTema:
                self.populateComboBox(self.classiPrevisteCombo,self.classiXTema)
                self.classeAggiuntaCheck.setCheckState(Qt.Unchecked)
            else:
                self.populateComboBox(self.classiPrevisteCombo,[u"La geometria del Layer è incompatibile con le Classi standard"])
                #self.enabledisableClassiAggiunte(True)
                self.classeAggiuntaCheck.setCheckState(Qt.Checked)

            self.show()

    def associaATema(self,layer,temaDirPath):
        self.resetDialog()
        self.temaDirPath = temaDirPath
        self.temaDir = QFileInfo(temaDirPath)
        self.codTema = self.temaDir.completeBaseName()[:5]
        self.sintesiClassi = self.parent.metadataTool.sintesiClassiR.keys()
        if layer:
            self.filesPath = []
            self.cLayer = layer
            self.cLayerName = self.cLayer.name()
            if self.cLayer.type() == QgsMapLayer.VectorLayer:
                self.associaFunc = self.associaSHP
                if self.cLayer.geometryType() == QGis.Point:
                    self.layerGeom = "3"
                elif self.cLayer.geometryType() == QGis.Line:
                    self.layerGeom = "2"
                elif self.cLayer.geometryType() == QGis.Polygon:
                    self.layerGeom = "1"
            elif self.cLayer.type() == QgsMapLayer.RasterLayer:
                self.layerGeom = "0"
                self.associaFunc = self.associaTIF
        else:
            self.filesPath, filefilter = QFileDialog.getOpenFileNamesAndFilter(None,
                                                                   "selezionare i files da aggiungere alla classe",
                                                                   self.parent.settaggi.path_classi, '*.zip *.jpg *.xls, *.doc *.pdf *.txt')
            if self.filesPath:
                self.associaFunc = self.associaDATASET
                if len(self.filesPath) == 1:
                    self.layerGeom = "0"
                else:
                    self.layerGeom = "4"
            else:
                return

        self.classiXTema = filter(self.filtraClassi,self.sintesiClassi)

        print "self.classiXTema", self.classiXTema

        if self.classiXTema:
            self.populateComboBox(self.classiPrevisteCombo,self.classiXTema)
            self.classeAggiuntaCheck.setCheckState(Qt.Unchecked)
            self.enabledisableClassiAggiunte(False)
        else:
            self.populateComboBox(self.classiPrevisteCombo,[u"La geometria del Layer è incompatibile con le Classi standard"])
            #self.enabledisableClassiAggiunte(True)
            self.classeAggiuntaCheck.setCheckState(Qt.Checked)
            self.enabledisableClassiAggiunte(True)

        self.show()

    def associaSHP(self):
        targetFile = QFileInfo(os.path.join(self.temaDirPath,self.nomeLabel.text()+".shp"))
        if targetFile.exists():
            reply = QMessageBox.question(None,"Il file %s è già esistente. Sovrascrivere?" % self.nomeLabel.text()+".shp", QMessageBox.Yes, QMessageBox.No)
            if reply == QMessageBox.Yes:
                scriviOK = True
            else:
                scriviOk = None
        else:
            scriviOK = True
        if scriviOK:
            QgsVectorFileWriter.writeAsVectorFormat(self.cLayer,targetFile.filePath(), 'utf-8', self.cLayer.crs())

    def associaTIF(self):
        targetFile = QFileInfo(os.path.join(self.temaDirPath,self.nomeLabel.text()+".tif"))
        if targetFile.exists():
            reply = QMessageBox.question(None,"Il file %s è già esistente. Sovrascrivere?" % self.nomeLabel.text()+".shp", QMessageBox.Yes, QMessageBox.No)
            if reply == QMessageBox.Yes:
                scriviOK = True
            else:
                scriviOk = None
        else:
            scriviOK = True
        if scriviOK:
            wrtr = QgsRasterFileWriter(targetFile.filePath())
            provider = self.cLayer.dataProvider()
            pipe = QgsRasterPipe()
            pipe.set(provider.clone())
            res = wrtr.writeRaster(
                pipe,
                self.cLayer.width(),
                self.cLayer.height(),
                self.cLayer.extent(),
                self.cLayer.crs(),
            )

    def associaDATASET(self):
        if len(self.filesPath) == 1:
            targetFile = os.path.join(self.temaDirPath, self.nomeLabel.text() + "." + QFileInfo(self.filesPath[0]).suffix())
            shutil.copy(self.filesPath[0],targetFile)
        else:
            targetDir = os.path.join(self.temaDirPath,self.nomeLabel.text())
            os.mkdir(targetDir)
            for ds_file in self.filesPath:
                shutil.copy(ds_file, targetDir)

    def associa(self):
        self.associaFunc()

