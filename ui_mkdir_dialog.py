# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_mkdir_dialog.ui'
#
# Created: Mon Feb  9 16:09:51 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_mkdirDialog(object):
    def setupUi(self, mkdirDialog):
        mkdirDialog.setObjectName(_fromUtf8("mkdirDialog"))
        mkdirDialog.resize(183, 103)
        self.horizontalLayout = QtGui.QHBoxLayout(mkdirDialog)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label = QtGui.QLabel(mkdirDialog)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.sourceString = QtGui.QLineEdit(mkdirDialog)
        self.sourceString.setReadOnly(False)
        self.sourceString.setObjectName(_fromUtf8("sourceString"))
        self.verticalLayout.addWidget(self.sourceString)
        self.buttonBox = QtGui.QDialogButtonBox(mkdirDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout.addWidget(self.buttonBox)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout.addLayout(self.verticalLayout)

        self.retranslateUi(mkdirDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), mkdirDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), mkdirDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(mkdirDialog)

    def retranslateUi(self, mkdirDialog):
        mkdirDialog.setWindowTitle(_translate("mkdirDialog", "Dialog", None))
        self.label.setText(_translate("mkdirDialog", "Crea Directory", None))

