# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GDM - class GDM_albero
                                 A QGIS plugin
 compilazione assistita metadati regione veneto
                              -------------------
        begin                : 2014-12-04
        git sha              : $Format:%H$
        copyright            : (C) 2014 by Enrico Ferreguti
        email                : enricofer@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

#from PyQt4 import QtCore, QtGui

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4 import uic

from fuzzywuzzy import fuzz, process
#from ui_rename_dialog import Ui_renameDialog

import os

Ui_renameDialog, _ = uic.loadUiType(os.path.join(os.path.dirname(__file__), 'ui_rename_dialog.ui'))


class renameDialog(QDialog, Ui_renameDialog):
    def __init__(self, parent_class):
        QDialog.__init__(self)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.sintesiClassiR = parent_class.metaDialogTab.sintesiClassiR
        self.setupUi(self)
        self.buttonBox.accepted.connect(self.rename)
        self.buttonBox.rejected.connect(self.cancel)
        self.destinationString.textChanged.connect(self.updateHintList)
        self.suggerimentiList.itemClicked.connect(self.takeHint)
        font = QFont()
        font.setPointSize(8)
        #font.setWeight(75)
        self.suggerimentiList.setFont(font)

    def ren(self,sourcePath):
        #QMessageBox.critical(None, "", "arrivato!")
        self.show()
        #self.raise_()
        self.renameInfo = QFileInfo(sourcePath)
        self.sourceString.setText(self.renameInfo.baseName())
        self.destinationString.setText(self.renameInfo.baseName())
        self.destinationString.setFocus()
        self.destinationString.selectAll()

    def cancel(self):
        #self.hide()
        pass

    def rename(self):
        newName = self.destinationString.text()
        iter = self.getAllFileWithSameName(self.renameInfo.filePath())
        rollBack = None
        for iterFile in iter:
            file = QFile(os.path.join(self.renameInfo.absolutePath(),iterFile))
            fileInfo = QFileInfo(os.path.join(self.renameInfo.absolutePath(),iterFile))
            if not fileInfo.isDir():
                suffix = "."+fileInfo.completeSuffix()
            else:
                suffix = ""
            if not file.rename(os.path.join(self.renameInfo.absolutePath(),newName+suffix)):
                QMessageBox.critical(None, "Impossibile Rinominare la classe", u"il sistema operativo impedisce l'accesso al file " + iterFile )
                rollBack= True
                break
        if rollBack:
            rollBackiter = self.getAllFileWithSameName(os.path.join(self.renameInfo.absolutePath(),newName+"."+fileInfo.completeSuffix()))
            for iterFile in rollBackiter:
                file = QFile(os.path.join(self.renameInfo.absolutePath(),iterFile))
                fileInfo = QFileInfo(os.path.join(self.renameInfo.absolutePath(),iterFile))
                file.rename(os.path.join(self.renameInfo.absolutePath(),self.sourceString.text()+"."+fileInfo.completeSuffix()))


    def getAllFileWithSameName(self,fileName):
        fileInfo = QFileInfo(fileName)
        fileDir = fileInfo.absoluteDir()
        return fileDir.entryList([fileInfo.baseName()+"*"])

    def updateHintList(self,content):
        if content:
            distance_dict = {}
            self.suggerimentiList.clear()
            similars = process.extract(content, self.sintesiClassiR.keys(), limit=10)
            for idx in range(0,9):
                self.suggerimentiList.addItem(similars[idx][0])

    def takeHint(self,itemWidget):
        self.destinationString.setText(itemWidget.text())