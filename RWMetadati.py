# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GDM - class GDM_albero
                                 A QGIS plugin
 compilazione assistita metadati regione veneto
                              -------------------
        begin                : 2014-12-04
        git sha              : $Format:%H$
        copyright            : (C) 2014 by Enrico Ferreguti
        email                : enricofer@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtXml import *
from PyQt4.QtXmlPatterns import *
from qgis.core import *
from qgis.utils import *
from qgis.gui import *

import subprocess, os
import sys, csv, codecs, cStringIO
import re
#import rpdb2

dirty = None

responsibleParties = {}

codesList ={
"CI_DateTypeCode" :{
    "en":['creation', 'publication', 'revision'],
    "it":["Creazione","Pubblicazione","Revisione","Rilievo"]
 },
"CI_PresentationFormCode": {
    "en":['documentDigital', 'imageDigital', 'documentHardcopy', 'imageHardcopy', 'mapDigital', 'mapHardcopy', 'modelDigital', 'modelHardcopy', 'profileDigital', 'profileHardcopy', 'tableDigital', 'tableHardcopy', 'videoDigital', 'videoHardcopy'],
    "it":["Documento digitale","Documento cartaceo","Immagine digitale","Immagine cartacea","Mappa digitale","Mappa cartacea","Modello digitale","Modello fisico","Profilo digitale","Profilo cartaceo","Tabella digitale","Tabella cartacea","Video digitale","Video analogico"]
 },
"CI_RoleCode":{
    "en":['resourceProvider', 'custodian', 'owner', 'user', 'distributor', 'originator', 'pointOfContact', 'principalInvestigator', 'processor', 'publisher', 'author'],
    "it":["Fornitore della risorsa","Custode","Proprietario","Utente","Distributore","Ideatore","Punto di contatto","Responsabile principale delle ricerche","Responsabile del trattamento","Editore","Autore"]
},
"MD_CellGeometryCode":{
    "en":["point","area"],
    "it":["Punto","Area"]
 },
"MD_CharacterSetCode":{
    "en":["ucs2","ucs4","utf7","utf8","utf16","8859part1","8859part2","8859part3","8859part4","8859part5","8859part6","8859part7","8859part8","8859part9","8859part10","8859part11","(reserved for future use)","8859part13","8859part14","8859part15","8859part16","jis","shiftJIS"],
    "it":["ucs2","ucs4","utf7","utf8","utf16","8859part1","8859part2","8859part3","8859part4","8859part5","8859part6","8859part7","8859part8","8859part9","8859part10","8859part11","(reserved for future use)","8859part13","8859part14","8859part15","8859part16","jis","shiftJIS"]
 },
"MD_ClassificationCode":{
    "en":["unclassified","restricted","confidential","secret","topSecret"],
    "it":["Non classificato","Riservato","Riservatissimo","Segreto","Segretissimo"]
 },
"MD_CoverageContentTypeCode":{
    "en":["unclassified","image","thematicClassification","physicalMeasurement"],
    "it":["Immagine","Classificazione tematica","Misura fisica"]
 },
"MD_DimensionNameTypeCode":{
    "en":["row","column","vertical"],
    "it":["Riga","Colonna","Verticale (quota)"]
 },
"MD_MaintenanceFrequencyCode":{
    "en":['continual', 'daily', 'weekly', 'fortnightly', 'monthly', 'quarterly', 'biannually', 'annually', 'asNeeded', 'irregular', 'notPlanned', 'unknown'],
    "it":["In maniera continua continual","Giornalmente","Settimanalmente","Ogni quindici giorni","Mensilmente","Trimestralmente","Due volte all'anno","Annualmente","Quando necessario","Irregolarmente","Non pianificato","Sconosciuto"]
 },
"MD_PixelOrientationCode":{
    "en":["center","lowerLeft","lowerRight","upperRight","upperLeft"],
    "it":["Centro","In basso a sinistra","In basso a destra","In alto a destra","In alto a sinistra"]
 },
"MD_ReferenceSystemCode":{
    "en":["WGS84","ETRS89","ETRS89/ETRS-LAEA","ETRS89/ETRS-LCC","ETRS89/ETRS-TM32","ETRS89/ETRS-TM33","ETRS89/UTM-zone32N","ETRS89/UTM-zone33N","ROMA40/EST","ROMA40/OVEST","ED50/UTM 32N","ED50/UTM 33N","IGM95/UTM 32N","(Raffittimento nazionale del sistema ETRS89)","IGM95/UTM 33N","WGS84/UTM 32N","WGS84/UTM 33N","WGS84/UTM 34N","BESSEL/Cassini-Soldner","BESSEL/Sanson-Flamsteed","CATASTO / Locale","ROMA40","ROMA40/ROMA","ED50","IGM95","Rete Altimetrica Nazionale","WGS84/3D","Livello medio delle basse maree sizigiali","Livello medio delle alte maree sizigiali","Livello medio lago","ITRS","ITRFxx","IGSxx","IGb00","ETRF89","ETRF00"],
    "it":["WGS84","ETRS89","ETRS89/ETRS-LAEA","ETRS89/ETRS-LCC","ETRS89/ETRS-TM32","ETRS89/ETRS-TM33","ETRS89/UTM-zone32N","ETRS89/UTM-zone33N","ROMA40/EST","ROMA40/OVEST","ED50/UTM 32N","ED50/UTM 33N","IGM95/UTM 32N","(Raffittimento nazionale del sistema ETRS89)","IGM95/UTM 33N","WGS84/UTM 32N","WGS84/UTM 33N","WGS84/UTM 34N","BESSEL/Cassini-Soldner","BESSEL/Sanson-Flamsteed","CATASTO / Locale","ROMA40","ROMA40/ROMA","ED50","IGM95","Rete Altimetrica Nazionale","WGS84/3D","Livello medio delle basse maree sizigiali","Livello medio delle alte maree sizigiali","Livello medio lago","ITRS","ITRFxx","IGSxx","IGb00","ETRF89","ETRF00"]
 },
"RS_Identifier":{
    "en":["WGS84","ETRS89","ETRS89/ETRS-LAEA","ETRS89/ETRS-LCC","ETRS89/ETRS-TM32","ETRS89/ETRS-TM33","ETRS89/UTM-zone32N","ETRS89/UTM-zone33N","ROMA40/EST","ROMA40/OVEST","ED50/UTM 32N","ED50/UTM 33N","IGM95/UTM 32N","(Raffittimento nazionale del sistema ETRS89)","IGM95/UTM 33N","WGS84/UTM 32N","WGS84/UTM 33N","WGS84/UTM 34N","BESSEL/Cassini-Soldner","BESSEL/Sanson-Flamsteed","CATASTO / Locale","ROMA40","ROMA40/ROMA","ED50","IGM95","Rete Altimetrica Nazionale","WGS84/3D","Livello medio delle basse maree sizigiali","Livello medio delle alte maree sizigiali","Livello medio lago","ITRS","ITRFxx","IGSxx","IGb00","ETRF89","ETRF00"],
    "it":["WGS84","ETRS89","ETRS89/ETRS-LAEA","ETRS89/ETRS-LCC","ETRS89/ETRS-TM32","ETRS89/ETRS-TM33","ETRS89/UTM-zone32N","ETRS89/UTM-zone33N","ROMA40/EST","ROMA40/OVEST","ED50/UTM 32N","ED50/UTM 33N","IGM95/UTM 32N","(Raffittimento nazionale del sistema ETRS89)","IGM95/UTM 33N","WGS84/UTM 32N","WGS84/UTM 33N","WGS84/UTM 34N","BESSEL/Cassini-Soldner","BESSEL/Sanson-Flamsteed","CATASTO / Locale","ROMA40","ROMA40/ROMA","ED50","IGM95","Rete Altimetrica Nazionale","WGS84/3D","Livello medio delle basse maree sizigiali","Livello medio delle alte maree sizigiali","Livello medio lago","ITRS","ITRFxx","IGSxx","IGb00","ETRF89","ETRF00"]
 },
"MD_RestrictionCode":{
    "en":['copyright', 'patent', 'patentPending', 'trademark', 'license', 'intellectualPropertyRights', 'restricted', 'otherRestrictions','openData'],
    "it":[u"Proprietà intellettuale dei dati","Brevetto","In attesa di brevetto","Marchio registrato ","Licenza ",u"Sfruttamento economico della proprietà intellettuale",u"Dato a conoscibilità limitata","Altri vincoli","Dato pubblico"]
 },
"MD_ScopeCode":{
    "en":["dataset","series","service","tile"],
    "it":["Dataset","Serie","Servizio","Sezione"]
 },
"MD_SpatialRepresentationTypeCode":{
    "en":['vector', 'grid', 'textTable', 'tin', 'stereoModel', 'video'],
    "it":["Dati vettoriali","Dati raster","Tabella di dati","TIN","Modello stereoscopico","Video"]
 },
"MD_TopicCategoryCode":{
    "en":["farming","biota","boundaries","climatologyMeteorologyAtmosphere","economy","elevation","environment","geoscientificInformation","Health","imageryBaseMapsEarthCover","intelligenceMilitary","inlandWaters","location","oceans","planningCadastre","society","structure","transportation","utilitiesCommunication"],
    "it":["Agricoltura","Biota","Confini","Climatologia - Meteorologia - Atmosfera","Economia","Elevazione","Ambiente","Informazioni","Salute","Mappe di base - Immagini - Copertura","Intelligence - Settore militare","Acque interne","Localizzazione","Acque marine - Oceani","Pianificazione - Catasto",u"Società","Strutture","Trasporti",u"Servizi di pubblica utilità"]#"Acque interne","Localizzazione","Acque marine - Oceani","Pianificazione - Catasto",u"Società","Strutture","Trasporti",u"Servizi di pubblica utilità",
 },
"MD_GeometricObjectTypeCode":{
"en":["point","line","area"],
"it":["punto","linea","area"]
 },
"MD_FileFormat":{
    "en":["folder","dataset","shp","pdf","doc","odt","txt","xls","dwg","dxf","tif","jpg","png","zip"],
    "it":["cartella","dataset","shp","pdf","doc","odt","txt","xls","ods","csv","dwg","dxf","tif","jpg","png","zip"]
 },
"":{
"en":[],
"it":[]
 }
}

def codeListValue(nod):
    attrs = nod.attributes()
    #print "ATTRS:",attrs.count()
    if attrs.namedItem("codeListValue"):
        return attrs.namedItem("codeListValue").nodeValue()
    else:
        return


def getCodeList(idCodList):
    if idCodList in codesList.keys():
        #print "Trovata codelist:",idCodList
        return codesList[idCodList]['it']
    elif idCodList == "responsibleParties":
        #print "responsibleParties:::"
        result = responsibleParties.keys()
        result.append("COLLEZIONA ENTE")
        return result
    #elif idCodList == "RS_Identifier":
    #    print "RS_IdentifierCode:::"
    #    return ["Error: codeList not found"]
    else:
        #print "combo errrato!!!"
        return ["Error: codeList not found"]

class UTF8Recoder:
    """
    Iterator that reads an encoded stream and reencodes the input to UTF-8
    """
    def __init__(self, f, encoding):
        self.reader = codecs.getreader(encoding)(f)

    def __iter__(self):
        return self

    def next(self):
        return self.reader.next().encode("utf-8")

class UnicodeReader:
    """
    A CSV reader which will iterate over lines in the CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        f = UTF8Recoder(f, encoding)
        self.reader = csv.reader(f, dialect=dialect, **kwds)

    def next(self):
        row = self.reader.next()
        return [unicode(s, "utf-8") for s in row]

    def __iter__(self):
        return self

class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)

class GrowingTextEdit(QTextEdit):

    def __init__(self, *args, **kwargs):
        super(GrowingTextEdit, self).__init__(*args, **kwargs)
        self.document().contentsChanged.connect(self.sizeChange)

        self.heightMin = 0
        self.heightMax = 65000

    def sizeChange(self):
        docHeight = self.document().size().height()
        if self.heightMin <= docHeight <= self.heightMax:
            self.setMinimumHeight(docHeight)

@pyqtSlot()
def currentIndexChanged(self):
    self.commitData.emit(self.sender())

class MetaItemDelegate(QItemDelegate):
    def __init__(self, parent, parentClass):
        self.parentClass = parentClass
        QItemDelegate.__init__(self, parent)
        self.parent = parent
        self.setDirty = self.parentClass.setDirty
        self.isDirty = self.parentClass.isDirty
        #print "PARENT:",parent


    def createEditor(self, parent, option, index):
        rowType = index.sibling(index.row(),4).data()
        if index.column() != 1:
            #print "invalid!"
            return
        #columnSize = parent.columnWidth(index.column())
        #print rowType,columnSize


        if rowType[:5] == "combo":
            codeList = rowType[6:].replace(']','')
            editWidget = QComboBox(parent)
            if codeList == "bounds":
                tipoDato = self.parentClass.checkDataFormat(self.parentClass.widget.XMLFilename.text())
                predef = index.sibling(index.row(),1).data() or ""
                if tipoDato == "shp":
                    li = [predef,"Carica le coordinate del dato","Carica le coordinate predefinite"]
                else:
                    li = [predef,"Carica le coordinate predefinite"]
            elif codeList == "identifi":
                li = [index.sibling(index.row(),1).data() or '',"l'identificatore corrisponde al nome del metadato"]
            else:
                li = getCodeList(codeList)
            print "CODELIST", codeList, li
            editWidget.addItems(li)
            editWidget.setEditable(True)
            editWidget.setCurrentIndex(0)
            editWidget.setMinimumHeight(25)
            editWidget.setMinimumWidth(0)
            editWidget.setMaximumWidth(200)
            #editWidget.setBaseSize(columnSize,editWidget.size().height)
            editWidget.setSizePolicy(QSizePolicy(QSizePolicy.Expanding,QSizePolicy.Minimum))
            editWidget.setSizeAdjustPolicy(QComboBox.AdjustToContents)
            self.connect(editWidget, SIGNAL("currentIndexChanged(int)"),
                         self, SLOT("currentIndexChanged()"))
        elif rowType[:4] == "date":
            editWidget = QDateEdit(parent)
            editWidget.setDisplayFormat("yyyy-MM-dd")
            editWidget.setMinimumHeight(25)
            editWidget.setSizePolicy(QSizePolicy(QSizePolicy.Expanding,QSizePolicy.Minimum))
        else:
            editWidget=GrowingTextEdit(parent)
            editWidget.setMaximumHeight(1000)
            #editWidget.setMaximumWidth(100)
            editWidget.setMinimumHeight(100)
            #editWidget.setMinimumWidth(90)
            editWidget.setSizePolicy(QSizePolicy(QSizePolicy.Expanding,QSizePolicy.Ignored))
            self.connect(editWidget, SIGNAL("textChanged()"),
                         self, SLOT("currentIndexChanged()"))
        return editWidget

    def setEditorData(self, editor, index):

        #print editor.metaObject().className()
        if index.column() == 1:
            editor.blockSignals(True)
            if editor.metaObject().className() == "QComboBox":
                if index.data():
                    editor.setEditText (index.data())
                else:
                    editor.setEditText ("")
            elif editor.metaObject().className() == "QDateEdit":
                if index.data():
                    editor.setDate(QDate().fromString(index.data(),"yyyy-MM-dd"))
                else:
                    editor.setDate(QDate().currentDate())
            else:
                if index.data():
                    editor.setText(index.data())
                else:
                    editor.setText("")
            editor.blockSignals(False)


    def setModelData(self, editor, model, index):
        if editor.metaObject().className() == "QComboBox":
            #print "CONTENT:", editor.currentText()
            content = editor.currentText()
            rowType = index.sibling(index.row(),4).data()
            if rowType == "combo[responsibleParties]":
                indexUrl = index.sibling(index.row()+2,0).child(0,1)
                indexTel = index.sibling(index.row()+2,0).child(1,1)
                indexEmail = index.sibling(index.row()+2,0).child(2,1)
                if content in responsibleParties:
                    model.setData(indexUrl, responsibleParties[content]["url"])
                    model.setData(indexTel, responsibleParties[content]["telefono"])
                    model.setData(indexEmail, responsibleParties[content]["email"])
                if content == "COLLEZIONA ENTE":
                    #print index.data(),indexUrl.data(),indexTel.data(),indexEmail.data()
                    self.parentClass.parent.gestioneSettaggi.aggiungiEnte(index.data(),indexUrl.data(),indexTel.data(),indexEmail.data())
                    self.parentClass.parent.gestioneSettaggi.updateDb()
                    content = "%%&&%%"#u'|!"£'
            elif rowType == "combo[bounds]":
                if content == "Carica le coordinate del dato":
                    currentXMLInfo = QFileInfo(str(self.parentClass.widget.XMLFilename.text()))
                    #QMessageBox.about(None,"",os.path.join(currentXMLInfo.absolutePath(),currentXMLInfo.completeBaseName()+'.shp'))
                    shpFileInfo = QFileInfo(os.path.join(currentXMLInfo.absolutePath(),currentXMLInfo.completeBaseName()+'.shp'))
                    shpInfo = self.parentClass.getSHPInfo(shpFileInfo.filePath())
                    west = str(shpInfo["ovest"]);east = str(shpInfo["est"]); south = str(shpInfo["sud"]);north = str(shpInfo["nord"])
                    content = "%%&&%%"#u'|!"£'
                elif content == "Carica le coordinate predefinite":
                    west, north, east, south = self.parentClass.parent.estensione_predefinita.corrente()
                    #west = str(self.parentClass.parent.dock.estensione.currentExtent().xMinimum())
                    #east = str(self.parentClass.parent.dock.estensione.currentExtent().xMaximum())
                    #south = str(self.parentClass.parent.dock.estensione.currentExtent().yMinimum())
                    #north = str(self.parentClass.parent.dock.estensione.currentExtent().yMaximum())
                    content = "%%&&%%"#u'|!"£'
                #else:
                #    west = "";east = ""; south = "";north = ""
                #QMessageBox.about(None,"","%s %s %s %s" % (west,east,north,south))
                if content == "%%&&%%":#u'|!"£':
                    indexWest = index.parent().child(0,1)
                    indexEast = index.parent().child(1,1)
                    indexSouth= index.parent().child(2,1)
                    indexNorth= index.parent().child(3,1)
                    model.setData(indexWest, west)
                    model.setData(indexEast, east)
                    model.setData(indexSouth, south)
                    model.setData(indexNorth, north)

            elif rowType == "combo[identifi]":
                node, metadataFileName_ = self.parentClass.XMLpath(self.parentClass.XMLdef, "//gmd:fileIdentifier//gco:CharacterString")
                metadataFileName = model.data(model.index(0,0).child(0,1))
                print content, metadataFileName_, metadataFileName
                if content == "l'identificatore corrisponde al nome del metadato":

                    model.setData(index, metadataFileName)
                    content = "%%&&%%"

            #model.setData(index, editor.currentText())
        elif editor.metaObject().className() == "QDateEdit":
            content = editor.text()
            #model.setData(index, editor.text())
        else:
            content = editor.toPlainText()


        if content != "%%&&%%":#'|!"£': #non applica il valore all'item se combo bounds e combo responsible party
            model.setData(index, content)
            self.setDirty(True)
        self.parentClass.traverseTree(action="applyTree")
        item = self.parent.itemFromIndex (index)
        item.setBackground(1,QBrush(QColor(220,236,236)))

    def setData(self, index, value, role=Qt.DisplayRole):
        print "setData", index.row(), index.column(), value
        #

    #def sizeHint


class dMetadati:

    def __init__(self,parent):
        self.parent = parent
        widget = parent.dock
        self.widget = widget
        self.tree = widget.alberoMetadati
        self.tabs = widget.tabWidget
        self.tab = widget.tabMetadati
        self.metaFilenameInput = widget.XMLFilename
        self.metaFilenameSearch = widget.openXMLfile
        self.tree.hideColumn(2)
        self.tree.hideColumn(3)
        self.tree.hideColumn(4)
        self.tree.hideColumn(5)
        self.tree.hideColumn(6)
        customItemDelegate = MetaItemDelegate(self.tree,self)
        self.tree.setItemDelegate(customItemDelegate)
        self.shpInfoCache = {}

        self.tree.setColumnWidth(0,100)
        self.tree.resizeColumnToContents(0)
        #self.metaFilenameInput.textChanged.connect(self.refreshTree)
        self.metaFilenameSearch.clicked.connect(self.openMetafile)
        widget.expandButton.clicked.connect(self.expandAction)
        widget.contractButton.clicked.connect(self.contractAction)
        widget.saveMetadataButton.clicked.connect(self.saveMetadataAction)
        widget.saveasMetadataButton.clicked.connect(self.saveAsMetadataAction)
        widget.undoMetadataEdits.clicked.connect(self.undoMetadataEdits)
        widget.saveTemplateButton.clicked.connect(self.saveTemplateAction)
        widget.loadTemplateButton.clicked.connect(self.loadTemplateAction)
        widget.validateButton.clicked.connect(self.validateAction)
        widget.autosettingButton.clicked.connect(self.autosettingAction)
        widget.resetButton.clicked.connect(self.resetAction)
        XMLFile = open(os.path.join(os.path.dirname(__file__),"gmxCodelists.xml"), "r")
        self.XMLcodedef = QDomDocument("")
        self.XMLcodedef.setContent(XMLFile.read())
        XMLFile.close()
        self.iface = parent.iface
        self.s = QSettings(os.path.join(os.path.dirname(__file__),".GDM"),QSettings.IniFormat)
        #default domdoc
        XMLFile = open(os.path.join(os.path.dirname(__file__),"default.xml"), "r")
        self.XMLdefault = QDomDocument("")
        self.XMLdefault.setContent(XMLFile.read())
        self.XMLdef = self.XMLdefault.cloneNode(True).toDocument()
        self.widget.control.setText(self.XMLdef.toString(2))
        self.traverseTree(action="reset")
        self.traverseTree(action="load")
        XMLFile.close()
        self.readSintesiClassi()

        #associa la lista dei responsibleParties
        global responsibleParties
        responsibleParties = self.parent.gestioneSettaggi.enti


    def setResponsibleParties(self,db):
        global responsibleParties
        responsibleParties = db

    def applyModsAction(self,item,column):
        self.traverseTree(action="applyTree")
        #self.validateTree()

    def trackMods(self,enable):
        pass

    def extrackMods(self,enable):
        if enable:
            try:
                self.tree.itemChanged.connect(self.applyModsAction)
            except:
                print "già connesso"
                pass
        else:
            try:
                self.tree.itemChanged.disconnect(self.applyModsAction)
            except:
                print "già disconnesso"
                pass

    def expandAction(self):
        self.tree.expandAll()

    def contractAction(self):
        current = self.tree.currentIndex()
        self.tree.collapseAll()

    def setDirty(self,val):
        global dirty
        if val:
            dirty = True
            dirtyIcon = QIcon(os.path.join(os.path.dirname(__file__),"icons","dirty.png"))
        else:
            dirty = False
            dirtyIcon = QIcon(os.path.join(os.path.dirname(__file__),"icons","clean.png"))
        self.parent.dock.tabWidget.setIconSize(QSize(10,20))
        self.parent.dock.tabWidget.setTabIcon(2,dirtyIcon)

    def isDirty(self):
        global dirty
        return dirty


    def validateTree(self,validateFile = None, highlight = True):
        #self.saveMetadataAction()
        #self.setDirty(False)
        if self.isDirty():
            self.iface.messageBar().pushMessage("Attenzione", "Modifiche ai metadati non salvate, validazione interrotta",level=QgsMessageBar.WARNING)
            return
        if validateFile:
            self.openMetadata(validateFile)
        Nod,tipoDoc = self.XMLpath(self.XMLdef,"//identificationInfo//MD_DataIdentification//citation//CI_Citation//presentationForm//CI_PresentationFormCode")
        Nod,tipoRappr = self.XMLpath(self.XMLdef,"//identificationInfo//MD_DataIdentification//spatialRepresentationType//MD_SpatialRepresentationTypeCode")
        if tipoDoc == u"Mappa digitale" or tipoDoc == u"Mappa cartacea" or tipoDoc == u"Modello digitale" or tipoDoc == u"Modello cartaceo":
            self.isMap = True
        else:
            self.isMap = None
        if tipoRappr == "Dati vettoriali":
            self.isRaster = None
            self.isVector = True
        elif tipoRappr == "Dati raster":
            self.isRaster = True
            self.isVector = None
        else:
            self.isRaster = None
            self.isVector = None
        self.validMetadata = True
        self.traverseTree(action="validate", highlight = highlight)
        return self.validMetadata

    def validateAction(self):
        #print self.validateTree()
        print self.validaFileMetadato(self.metaFilenameInput.text(),evidenzia=True)
        print "isMap:",self.isMap
        print "isVector:",self.isVector
        print "isRaster:",self.isRaster
        print "ValidName:",self.validateMetadataName()

    def validateMetadataName(self,validateFile = None):
        if validateFile:
            metadataName = QFileInfo(validateFile).completeBaseName()
        else:
            node,metadataName = self.XMLpath(self.XMLdef,"//fileIdentifier//gco:CharacterString")
        #print metadataName
        if metadataName.lower() in self.sintesiClassi_compare:
            return True
        else:
            return None

    def exvalidateAction(self):
        localSchemaUrl = QUrl()
        #schemaUrl = localSchemaUrl.fromLocalFile(os.path.join(os.path.dirname(__file__),"RNDT_XSD","ISO_19139_Schemas","ITgmd","ITgmd.xsd"))
        schemaUrl = QUrl("http://www.isotc211.org/schemas/2005/gmd/gmd.xsd")
        #print schemaUrl
        schema = QXmlSchema()
        schema.load(schemaUrl)
        schema.messageHandler()
        xmlFile = QFile(self.metaFilenameInput.text())
        xmlFile.open(QIODevice.ReadOnly)
        localXmlUrl = QUrl()
        localUrl = localXmlUrl.fromLocalFile(self.metaFilenameInput.text())
        print localUrl
        if schema.isValid():
            validator = QXmlSchemaValidator(schema)
            if validator.validate(xmlFile,localUrl):
                print "VALIDO!"
            else:
                print "NON VALIDO!"

    def checkDataFormat(self,filePath):
        fileInfo = QFileInfo(filePath)
        #applica tipo di dati:
        checkAbsoluteBaseName = os.path.join(fileInfo.absolutePath(),fileInfo.baseName())
        checkXML = QFileInfo(checkAbsoluteBaseName+".xml")
        checkPDF = QFileInfo(checkAbsoluteBaseName+".pdf")
        checkXLS = QFileInfo(checkAbsoluteBaseName+".xls")
        checkSHP = QFileInfo(checkAbsoluteBaseName+".shp")
        checkDOC = QFileInfo(checkAbsoluteBaseName+".doc")
        checkZIP = QFileInfo(checkAbsoluteBaseName+".zip")
        checkJPG = QFileInfo(checkAbsoluteBaseName+".jpg")
        checkPNG = QFileInfo(checkAbsoluteBaseName+".png")
        checkTIF = QFileInfo(checkAbsoluteBaseName+".tif")
        checkDIR = QFileInfo(fileInfo.absolutePath())

        tipo = "sconosciuto"

        if checkXML.exists():
            tipo = "xml"
        if checkDIR.exists() and checkDIR.isDir():
            tipo = "dataset"
        if checkPDF.exists():
            tipo = "pdf"
        if checkZIP.exists():
            tipo = "zip"
        if checkXLS.exists():
            tipo = "xls"
        if checkDOC.exists():
            tipo = "doc"
        if checkSHP.exists():
            tipo = "shp"
        if checkJPG.exists():
            tipo = "jpg"
        if checkTIF.exists():
            tipo = "tif"
        if checkPNG.exists():
            tipo = "png"

        return tipo



    def autosettingAction(self, newMetadata = None):
        self.setDirty(True)

        check = QFileInfo(os.path.join(os.path.dirname(__file__),"Templates","default.xml"))
        if check.exists():
            self.loadTemplateAction(fileName = check.absoluteFilePath(), overwrite = None)

        if newMetadata:
            current = QFileInfo(newMetadata)
            self.metaFilenameInput.setText(newMetadata)
        else:
            current = QFileInfo(self.metaFilenameInput.text())
        self.XMLpath(self.XMLdef,"//fileIdentifier//gco:CharacterString",current.completeBaseName())

        tipo = self.checkDataFormat(self.metaFilenameInput.text())

        self.XMLpath(self.XMLdef,"//gmd:language//gco:CharacterString","ita", overwrite = True)
        self.XMLpath(self.XMLdef,"//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:language//gco:CharacterString","ita", overwrite = True)

        ext = ""

        if tipo == 'cartella':
            self.XMLpath(self.XMLdef,"//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:citation//gmd:CI_Citation//gmd:presentationForm//gmd:CI_PresentationFormCode","Documento digitale", overwrite = None)
            self.XMLpath(self.XMLdef,"//gmd:distributionInfo//gmd:MD_Distribution//gmd:distributionFormat//gmd:MD_Format//gmd:name//gco:CharacterString","cartella", overwrite = None)

        if tipo == 'pdf':
            ext = ".pdf"
            self.XMLpath(self.XMLdef,"//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:citation//gmd:CI_Citation//gmd:presentationForm//gmd:CI_PresentationFormCode","Documento digitale", overwrite = None)
            #self.XMLpath(self.XMLdef,"","")
            self.XMLpath(self.XMLdef,"//gmd:distributionInfo//gmd:MD_Distribution//gmd:distributionFormat//gmd:MD_Format//gmd:name//gco:CharacterString","pdf", overwrite = None)

        if tipo == 'xls':
            ext = ".xls"
            self.XMLpath(self.XMLdef,"//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:citation//gmd:CI_Citation//gmd:presentationForm//gmd:CI_PresentationFormCode","Tabella digitale", overwrite = None)
            self.XMLpath(self.XMLdef,"//gmd:distributionInfo//gmd:MD_Distribution//gmd:distributionFormat//gmd:MD_Format//gmd:name//gco:CharacterString","xls", overwrite = None)

        if tipo == 'zip':
            ext = ".zip"
            self.XMLpath(self.XMLdef,"//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:citation//gmd:CI_Citation//gmd:presentationForm//gmd:CI_PresentationFormCode","Documento digitale", overwrite = None)
            self.XMLpath(self.XMLdef,"//gmd:distributionInfo//gmd:MD_Distribution//gmd:distributionFormat//gmd:MD_Format//gmd:name//gco:CharacterString","zip", overwrite = None)

        if tipo == 'doc':
            ext = ".doc"
            self.XMLpath(self.XMLdef,"//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:citation//gmd:CI_Citation//gmd:presentationForm//gmd:CI_PresentationFormCode","Documento digitale", overwrite = None)
            self.XMLpath(self.XMLdef,"//gmd:distributionInfo//gmd:MD_Distribution//gmd:distributionFormat//gmd:MD_Format//gmd:name//gco:CharacterString","doc", overwrite = None)

        if tipo == 'shp':
            ext = ".shp"
            self.XMLpath(self.XMLdef,"//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:citation//gmd:CI_Citation//gmd:presentationForm//gmd:CI_PresentationFormCode","Mappa digitale", overwrite = None)
            self.XMLpath(self.XMLdef,"//gmd:distributionInfo//gmd:MD_Distribution//gmd:distributionFormat//gmd:MD_Format//gmd:name//gco:CharacterString","shp", overwrite = None)
            #self.XMLpath(self.XMLdef,"//identificationInfo//MD_DataIdentification//spatialRepresentationType//MD_SpatialRepresentationTypeCode","Dati vettoriali", overwrite = None)

            SHPInfo = self.getSHPInfo(current.absolutePath()+current.completeBaseName()+".shp")
            self.XMLpath(self.XMLdef,"//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:extent//gmd:EX_Extent//gmd:geographicElement//gmd:EX_GeographicBoundingBox//gmd:westBoundLongitude//gco:Decimal",str(SHPInfo["ovest"]), overwrite = True)
            self.XMLpath(self.XMLdef,"//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:extent//gmd:EX_Extent//gmd:geographicElement//gmd:EX_GeographicBoundingBox//gmd:eastBoundLongitude//gco:Decimal",str(SHPInfo["est"]), overwrite = True)
            self.XMLpath(self.XMLdef,"//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:extent//gmd:EX_Extent//gmd:geographicElement//gmd:EX_GeographicBoundingBox//gmd:southBoundLatitude//gco:Decimal",str(SHPInfo["sud"]), overwrite = True)
            self.XMLpath(self.XMLdef,"//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:extent//gmd:EX_Extent//gmd:geographicElement//gmd:EX_GeographicBoundingBox//gmd:northBoundLatitude//gco:Decimal",str(SHPInfo["nord"]), overwrite = True)
            #self.XMLpath(self.XMLdef,"//spatialRepresentationInfo//MD_VectorSpatialRepresentation//geometricObjects//MD_GeometricObjects//geometricObjectType//MD_GeometricObjectTypeCode",str(SHPInfo["geom"]), overwrite = None)

        if tipo == "tif" or tipo == "png" or tipo == "jpg":
            ext = "."+tipo
            self.XMLpath(self.XMLdef,"//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:citation//gmd:CI_Citation//gmd:presentationForm//gmd:CI_PresentationFormCode","Immagine digitale", overwrite = None)
            self.XMLpath(self.XMLdef,"//gmd:distributionInfo//gmd:MD_Distribution//gmd:distributionFormat//gmd:MD_Format//gmd:name//gco:CharacterString",tipo, overwrite = None)


        #self.XMLpath(self.XMLdef,"//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:citation//gmd:CI_Citation//gmd:identifier//gmd:RS_Identifier//gmd:code//gco:CharacterString",current.completeBaseName()+ext, overwrite = True)
        parentDir = current.absoluteDir()
        self.XMLpath(self.XMLdef,"//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:citation//gmd:CI_Citation//gmd:series//gmd:CI_Series//gmd:issueIdentification//gco:CharacterString",parentDir.dirName(), overwrite = True)
        self.traverseTree(action="applyXML")


    def getSHPInfo(self,filePath):
        if filePath in self.shpInfoCache.keys():
            shpInfo = self.shpInfoCache[filePath]
            return {"geom":shpInfo[0],"nord":shpInfo[1],"sud":shpInfo[2],"est":shpInfo[3],"ovest":shpInfo[4]}
        shpFileInfo = QFileInfo(filePath)
        if shpFileInfo.exists():
            settings = QSettings()
            # Take the "CRS for new layers" config, overwrite it while loading layers and...
            oldProjValue = settings.value( "/Projections/defaultBehaviour", "prompt")
            settings.setValue( "/Projections/defaultBehaviour", "useProject" )
            vlayer = QgsVectorLayer(filePath, "autocompile_helper", "ogr")
            self.iface.messageBar().clearWidgets()
            crsSrc = QgsCoordinateReferenceSystem(3003)
            vlayer.setCrs(crsSrc)
            crsDst = QgsCoordinateReferenceSystem(4326)
            xform = QgsCoordinateTransform(crsSrc, crsDst)
            WGS84box = xform.transform(vlayer.extent())
            if vlayer.geometryType() == QGis.Point:
                tipo = "punto"
            elif vlayer.geometryType() == QGis.Line:
                tipo = "linea"
            elif vlayer.geometryType() == QGis.Polygon:
                tipo = "area"
            else:
                tipo = "unknown"
            settings.setValue( "/Projections/defaultBehaviour", oldProjValue )
            #QgsMapLayerRegistry.instance().removeMapLayer(vlayer.id())
            self.shpInfoCache[filePath] = [tipo, WGS84box.yMaximum(), WGS84box.yMinimum(), WGS84box.xMaximum(), WGS84box.xMinimum()]
            return {"geom":tipo,"nord":WGS84box.yMaximum(),"sud":WGS84box.yMinimum(),"est":WGS84box.xMaximum(),"ovest":WGS84box.xMinimum()}
        else:
            return {"geom":None,"nord":None,"sud":None,"est":None,"ovest":None}


    def resetAction(self):
        resetOk = None
        if self.isDirty():
            reply = QMessageBox.question(None,"Metadati non salvati","Le modifiche alla scheda dei metadati non sono ancora stati salvate. Cancellare i metadati?", QMessageBox.Yes, QMessageBox.No)
            if reply == QMessageBox.Yes:
                resetOk = True
        else:
            resetOk = True
        if resetOk:
            self.traverseTree(action="reset")
            self.setDirty(False)

    def newMetadata(self,fileName = None):
        Dlg=QFileDialog()
        Dlg.setDefaultSuffix("xml")
        self.resetAction()
        if not fileName:
            fileName = Dlg().getSaveFileName (None,"Salva nuovo Metadato vuoto", self.widget.control.text(), "*.xml")
        self.loadTemplateAction(os.path.join(os.path.dirname(__file__),"default.xml"))
        self.saveMetadata(fileName)
        self.metaFilenameInput.setText(fileName)



    def saveTemplateAction(self,saveAs = None):
        Dlg=QFileDialog()
        Dlg.setDefaultSuffix("xml")
        if saveAs:
            fileName = QFileDialog().getSaveFileName (None,"Salva scheda corrente in altro file di metadato", self.widget.control.text(), "*.xml")
        else:
            fileName = QFileDialog().getSaveFileName (None,"Salva scheda corrente come modello di metadato xml", os.path.join(os.path.dirname(__file__),"Templates"), "*.xml")#,QFileDialog.DontUseNativeDialog)
        #if QFileInfo(fileName).completeSuffix() != "xml":
        #    filename = QFileInfo(fileName).absolutePath()+QFileInfo(fileName).baseName()+".xml"
        if fileName:
            self.XMLTemplate = self.XMLdefault.cloneNode(True).toDocument()
            XMLFile = QFile(fileName)
            XMLFile.open(QFile.WriteOnly | QFile.Text)
            self.traverseTree(action="saveTemplate")
            self.widget.control.setText(self.XMLTemplate.toString(2))
            self.XMLTemplate.save(QTextStream(XMLFile),3,QDomNode.EncodingFromDocument)
            XMLFile.flush()
            XMLFile.close()

    def loadTemplateAction(self,fileName = None, overwrite = True):
        if not fileName:
            fileName = QFileDialog.getOpenFileName (None,"Inserisci modello di metadato xml nella scheda corrente", os.path.join(os.path.dirname(__file__),"Templates"), "*.xml")
        if fileName:
            XMLFile = open(fileName, "r")
            self.XMLTemplate = QDomDocument("")
            self.XMLTemplate.setContent(XMLFile.read())
            self.traverseTree(action="loadTemplate", overwrite = True)
            self.setDirty(True)
            XMLFile.close()
        self.trackMods(True)

    def codeListValues(self,nod):
        if codeListValue(nod):
            codeListElements = self.XMLcodedef.elementsByTagName("CodeListDictionary")
            for id in range (0,codeListElements.count()):
                #print "ID",codeListElements.item(id).attributes().namedItem("gml:id").nodeValue()
                if codeListElements.item(id).attributes().namedItem("gml:id").nodeValue() == nod.nodeName():
                    codeListElement = codeListElements.item(id)
            #try:
            #    print codeListElement.nodeValue()
            #except:
            #    return
            #print codeListElement.parentNode().nodeValue(), codeListElement.parentNode().parentNode().nodeValue()
            codeEntries = codeListElement.toElement().elementsByTagName("codeEntry")
            #description = codeListElement.firstChild()
            #identifier = description.nextSibling()
            #print "CODLIST:",nod.nodeName(),"#",codeEntries.count()
            identifiers = []
            for id in range(0,codeEntries.count()):
                identifierNodes = codeEntries.item(id).toElement().elementsByTagName("CodeDefinition").item(0).toElement().elementsByTagName('gml:identifier')
                if identifierNodes:
                    print identifierNodes.item(0).nodeValue(),"/",identifierNodes.item(0).firstChild().nodeValue()
                    identifiers.append(identifierNodes.item(0).firstChild().nodeValue())
            #print identifiers
            return identifiers
        else:
            return
        XMLFile.close()

    def saveMetadataAction(self):
        self.saveMetadata(self.metaFilenameInput.text())
        #self.validaFileMetadato(self.metaFilenameInput.text(),evidenzia=True)

    def saveAsMetadataAction(self):
        self.saveTemplateAction(saveAs = True)

    def saveMetadata(self,saveFile):
        self.XMLsave = self.XMLdefault.cloneNode(True).toDocument()
        XMLFile = QFile(saveFile)
        XMLFile.open(QFile.WriteOnly | QFile.Text)
        self.traverseTree(action="save")
        self.widget.control.setText(self.XMLsave.toString(2))
        self.XMLsave.save(QTextStream(XMLFile),3,QDomNode.EncodingFromDocument)#
        XMLFile.flush()
        XMLFile.close()
        self.setDirty(False)
        self.XMLdef = self.XMLsave.cloneNode(True).toDocument()
        self.traverseTree(action="clean")
        self.widget.log.clear()

    def undoMetadataEdits(self):
        self.setDirty(False)
        self.openMetadata(self.metaFilenameInput.text())

    def openMetadata(self,openFile):

        if self.isDirty():
            reply = QMessageBox.question(None,"Metadati non salvati","Le modifiche alla scheda dei metadati non sono ancora stati salvate. Salvare?", QMessageBox.Yes, QMessageBox.No)
            if reply == QMessageBox.Yes:
                self.saveMetadata(self.bakFileName)
            self.setDirty(False)
        #self.s.setValue("GDM/XMLfileState",QFileInfo(openFile).path())
        self.parent.gestioneSettaggi.setMetadato(QFileInfo(openFile).path())

        XMLFile = open(openFile, "r")
        self.XMLdef = QDomDocument("")
        self.XMLdef.setContent(XMLFile.read())
        self.traverseTree(action="load")
        XMLFile.close()
        self.metaFilenameInput.setText(openFile)
        #self.widget.control.setText(self.XMLdef.toString(2))
        self.widget.log.clear()

    def inspectMetadata(self,openFile):
        XMLFile = open(openFile, "r")
        self.XMLInspectDom = QDomDocument("")
        self.XMLInspectDom.setContent(XMLFile.read())
        self.XMLInspectFile = openFile
        XMLFile.close()


    def XMLInspection(self,metadataFile,NodePathList):
        self.inspectMetadata(metadataFile)
        res = []
        for NodePath in NodePathList:
            node,content = self.XMLpath(self.XMLInspectDom,NodePath)
            res.append(content)
        return res

    def openMetafile(self):
        self.bakFileName = self.metaFilenameInput.text()
        if  self.metaFilenameInput.text() == "":
            #default = self.s.value("GDM/XMLfileState","")
            default = self.parent.gestioneSettaggi.metadato
        else:
            default =QFileInfo(self.metaFilenameInput.text()).absolutePath()
        fileName = QFileDialog.getOpenFileName(None,"Apri metadato xml",default , "*.xml")
        if fileName:
            self.openMetadata(fileName)
            self.expandAction()
            self.validaFileMetadato(fileName,evidenzia=True)

    def XMLpath(self,domDoc,xPath,NV = "%%READMODE%%", overwrite = True):
        newValue = NV
        global codesList
        xList = xPath.split("//")
        xList = xList[1:]
        #print xList
        baseSearch = domDoc.documentElement()
        umlStyleValidator=QRegExp("^[g][a-z]{2}[:][a-zA-Z0-9_]{0,100}")
        for xNode in xList:
            AttrName = ""
            AttrValue = ""
            iterValue = ""
            if "##" in xNode:
                searchName,AttrName,AttrValue = xNode.split("##")
            elif "%%" in xNode:
                searchName,iterValue = xNode.split("%%")
            else:
                searchName = xNode

            if umlStyleValidator.exactMatch(searchName):
                searchName = searchName[4:]

            nodes = []
            children = baseSearch.childNodes()

            cicleNode = baseSearch.firstChild()
            #print cicleNode.namespaceURI()
            while not cicleNode.isNull() :
                #print cicleNode.parentNode().nodeName(),cicleNode.nodeName(),cicleNode.nextSibling().nodeName()
                cicleNode = cicleNode.nextSibling()

            #for child in range(0,children.count()):
            #    print "#",child,children.item(child).nodeName()
            for child in range(0,children.count()):
                if umlStyleValidator.exactMatch(children.item(child).nodeName()):
                    XMLNodeName = children.item(child).nodeName()[4:]
                else:
                    XMLNodeName = children.item(child).nodeName()
                if XMLNodeName == searchName:
                    if (AttrValue != "" and children.item(child).hasAttributes()):
                        idNode = children.item(child).attributes().namedItem(AttrName)
                        if idNode:
                            #print "NOME:",idNode.nodeName(),"VALORE ATTUALE:",idNode.nodeValue(),"VALORE RICERCATO:",AttrValue,"|","VALORE TARGET:",newValue
                            if umlStyleValidator.exactMatch(idNode.nodeValue()):
                                if idNode.nodeValue()[4:] == AttrValue:
                                    nodes.append(children.item(child))
                            else:
                                if idNode.nodeValue() == AttrValue:
                                    nodes.append(children.item(child))
                    else:
                        nodes.append(children.item(child))


            #print "NODES",nodes
            if len(nodes) == 0:
                #print "error. path not found:"#,xPath
                return (None,None)
            elif len(nodes) == 1:
                baseSearch = nodes[0]
            elif iterValue != "":
                try:
                   baseSearch = nodes[int(iterValue)]
                except:
                   pass

        #print "VALORE DEL NODO:",baseSearch,baseSearch.firstChild().nodeValue(),"VALORE TARGET:",newValue
        if baseSearch and newValue != "%%READMODE%%":
            #print "WRITEMODE"
            if newValue != "" and not baseSearch.hasChildNodes():
                #print "setting new textnode"
                txtDom = domDoc.createTextNode("")
                baseSearch.appendChild(txtDom)
            if (baseSearch.firstChild().nodeValue() == "" and not overwrite) or overwrite:
                try:
                    baseSearch.firstChild().setNodeValue(newValue)
                    #print baseSearch.nodeName()
                    if baseSearch.nodeName() in codesList:
                        #print "CODELIST!"
                        codeListValueNode = baseSearch.attributes().namedItem("codeListValue")
                        try:
                            indexCodeList = codesList[baseSearch.nodeName()]['it'].index(newValue)
                            codeListValueNode.setNodeValue(codesList[baseSearch.nodeName()]['en'][indexCodeList])
                        except:
                            #print "malformed"
                            if newValue != "":
                                newValue += " (MALFORMED)"
                            codeListValueNode.setNodeValue("")
                    if newValue == "" and baseSearch.firstChild().nodeValue() =="":
                        #print "removing null text node"
                        nullTextNode = baseSearch.firstChild()
                        baseSearch.removeChild(nullTextNode)
                    #print "scrittura effettuata!"
                    self.setDirty(True)
                except:
                    print "scrittura fallita!", sys.exc_info()[0]

        return (baseSearch,baseSearch.firstChild().nodeValue()) #returns a tuple(parentnode, content of firstchild)


    def traverseXML(self,branch,path):
        children = branch.childNodes()
        for child in range(0,children.count()):
            childPath = path + "//" + children.item(child).nodeName()
            if children.item(child).hasChildNodes():
                #if not children.item(child).firstChild().hasChildNodes():
                #    print "VALORE:",self.XMLpath(self.XMLdef,childPath)
                self.traverseXML(children.item(child),childPath)
            #print "VALORE:",self.XMLpath(self.XMLdef,childPath)

    def refreshTree(self):
        self.tabs.setCurrentWidget(self.tab)
        self.openMetadata(self.metaFilenameInput.text())

    def traverseTree(self,action="clean",keys = None,color = None,highlight = True,overwrite = True):
        #print action
        self.recurseTree(self.tree.invisibleRootItem(),action,keys,color,highlight,overwrite)

    def recurseTree(self,branch,action,keys,color,highlight,overwrite):
        child_count = branch.childCount()
        for i in range(child_count):
            item = branch.child(i)
            if action == "load":
                if item.text(2) != "":
                    if item.text(3) != "d":
                        itemNode,itemContent0 = self.XMLpath(self.XMLdef,item.text(5))#leggi metadati versione vecchia
                        itemNode,itemContent1 = self.XMLpath(self.XMLdef,item.text(2))#leggi metadati versione nuova
                        #print itemContent0,itemContent1
                        if itemContent1:
                            itemContent = itemContent1
                        else:
                            itemContent = itemContent0
                        item.setText(1,itemContent)
                        item.setBackground(1,QBrush(QColor(255,255,255)))
                    else:
                        item.setForeground(1,QBrush(QColor(125,125,125)))
                        item.setForeground(0,QBrush(QColor(125,125,125)))
            elif action == "clean":
                item.setBackground(1,QBrush(QColor(255,255,255)))
            elif action == "reset":
                if item.text(3) != "d":
                    item.setBackground(1,QBrush(QColor(255,255,255)))
                    item.setToolTip(0,item.text(6))
                    item.setText(1,"")
                else:
                    item.setBackground(1,QBrush(QColor(200,200,255)))
            elif action == "save":
                if item.text(2) != "":
                    #self.XMLpath(self.XMLdef,item.text(2),item.text(1))
                    self.XMLpath(self.XMLsave,item.text(2),item.text(1))
                    #print item.text(0),item.text(1)
            elif action == "saveTemplate":
                if item.text(2) != "":
                    self.XMLpath(self.XMLTemplate,item.text(2),item.text(1))
            elif action == "applyXML":
                if item.text(2) != "":
                    itemNode,itemContent = self.XMLpath(self.XMLdef,item.text(2))
                    if item.text(1) != itemContent: #applica solo i valori dell'albero xml che differiscono da quello visualizzato
                        item.setText(1,itemContent)
                        item.setBackground(1,QBrush(QColor(220,236,236)))
            elif action == "applyTree":
                if item.text(2) != "":
                    itemNode,itemContent = self.XMLpath(self.XMLdef,item.text(2))
                    if item.text(1) != itemContent: #applica solo i valori visualizzati che differiscono da quelli dell'albero xml
                        self.XMLpath(self.XMLdef,item.text(2),item.text(1))
                        item.setBackground(1,QBrush(QColor(220,236,236)))
                        #if item.text(3) != "d":
                        #    item.setBackground(1,QBrush(QColor(220,236,236)))
            elif action == "highlight":
                if keys:
                    if item.text(2)!= "" and item.text(2) in keys:
                        #print item.text(2)
                        if color:
                            item.setBackground(1,QBrush(QColor(color)))
                        else:
                            item.setBackground(1,QBrush(QColor(230,152,0)))
            elif action == "loadTemplate":
                if item.text(2) != "":
                    if item.text(3) != "d": #non sovrascrivere se valore di default
                        itemTemplateNode,itemTemplateContent = self.XMLpath(self.XMLTemplate,item.text(2))
                        if itemTemplateContent != "":
                            #print itemTemplateContent
                            if ((item.text(1) == "" or item.text(1) == None) and not overwrite) or overwrite:
                                item.setText(1,itemTemplateContent)
                                item.setBackground(1,QBrush(QColor(220,236,236)))
            elif action == "validate":
                if item.text(1) == "":
                    if item.text(3) == "1" or item.text(3) == "2" or (item.text(3) == "3" and self.isMap) or (item.text(3) == "4" and self.isRaster) or (item.text(3) == "5" and self.isVector):
                        self.validMetadata = None
                        item.setBackground(1,QBrush(QColor( 253, 89, 95 )))

            if not highlight:
                item.setBackground(1,QBrush(QColor( 255,255,255 )))

            if item.childCount()>0:
                self.recurseTree(item,action,keys,color,highlight,overwrite)

    def readSintesiClassi(self):
        csvFile = open(self.parent.gestioneSettaggi.path_classi, 'rb')
        csvReader = UnicodeReader(csvFile, delimiter='\t', quotechar='"')
        csvReader.next()
        self.sintesiClassiR={}
        self.sintesiClassi_compare=[]
        for row in csvReader:
            self.sintesiClassiR[row[0]]=row
            self.sintesiClassi_compare.append(row[0].lower())
        csvFile.close()

    def utf_8_encoder(self,unicode_csv_data):
        for line in unicode_csv_data:
            yield line.encode('utf-8')

    def validaFileMetadato(self,metaFile, evidenzia = None, log = True):
        log = ""
        errors = []
        errorsPath = []
        validMetafile = True
        metaFileInfo = QFileInfo(metaFile)
        metaFileName = metaFileInfo.completeBaseName()
        parentDirName = metaFileInfo.absoluteDir().dirName()
        tipoFormato = self.checkDataFormat(metaFile)
        if tipoFormato == "shp":
            dataFileInfo = QFileInfo(os.path.join(metaFileInfo.absolutePath(),metaFileInfo.baseName()+".shp"))
            if dataFileInfo.isFile():
                geometryInfo = self.getSHPInfo(dataFileInfo.absoluteFilePath())
                tipoGeom = geometryInfo["geom"]
            else:
                tipoGeom = None
        else:
            tipoGeom = None

        pathFormatoDato = "//gmd:distributionInfo//gmd:MD_Distribution//gmd:distributionFormat//gmd:MD_Format//gmd:name//gco:CharacterString"
        pathGeomDato = "//spatialRepresentationInfo//MD_VectorSpatialRepresentation//geometricObjects//MD_GeometricObjects//geometricObjectType//MD_GeometricObjectTypeCode"
        pathNomeDato = "//gmd:fileIdentifier//gco:CharacterString"
        pathIdDato = "//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:citation//gmd:CI_Citation//gmd:identifier//gmd:RS_Identifier//gmd:code//gco:CharacterString"
        pathContainer = "//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:citation//gmd:CI_Citation//gmd:series//gmd:CI_Series//gmd:issueIdentification//gco:CharacterString"

        XMLFile = open(metaFile, "r")
        XMLvalid = QDomDocument("")
        XMLvalid.setContent(XMLFile.read())
        XMLFile.close()

        nodo, tipoFormatoDichiarato = self.XMLpath(XMLvalid,pathFormatoDato)
        nodo, tipoGeomDichiarato = self.XMLpath(XMLvalid,pathGeomDato)
        node,metadataId = self.XMLpath(XMLvalid,pathIdDato)
        idName = QFileInfo(metadataId).completeBaseName()

        #rpdb2.setbreak()

        #log = u"METAFILENAME: %s\nPARENTDIRNAME: %s\nTIPODATO:%s\nTIPOFORMATO:%s\nFORMATODICH: %s\nGEOMDICH: %s" % (metaFileName,parentDirName,tipoDato,tipoFormato,tipoFormatoDichiarato,tipoGeomDichiarato)

        #Valutazione se la formattazione del file è quella ammessa:
        nameValidator = QRegExp("^[abc][0-9]{7}[_][A-Za-z0-9]{0,17}$")
        #nameValidator = re.compile("^[abc][0-9]{7}[_][A-Za-z]{0,17}$")
        #print nameValidator.match(metaFileName)

        try:
            prefisso = metaFileName[:9]
            descrizione = metaFileName[9:]
            tipoDato = metaFileName[7]
        except:
            log += "Errore 00: il formato del nome non corrisponde alle specifiche\n"
            errors.append("00")
            validMetafile = None
            return (validMetafile,log, errors)

        #if not nameValidator.match(metaFileName):
        if not nameValidator.exactMatch(metaFileName):
            prefixValidator = QRegExp("^[abc][0-9]{7}[_]$")
            suffixValidator = QRegExp("^[A-Za-z0-9]{0,100}")
            log += u"Errore 00: Il nome non è ben formato: "
            errors.append("00")
            if not prefixValidator.exactMatch(prefisso):
                log += u"il prefisso è errato; "
            if not suffixValidator.exactMatch(descrizione):
                log += u"La descrizione non contiene solo caratteri alfabetici; "
            if len(descrizione) > 17:
                log += u"La descrizione contiene più di 17 caratteri (%s); " % len(descrizione)
            log += "\n"
            validMetafile = None


        #Valutazione se il metadato è nella corretta posizione nell'albero:
        if metaFileName[:5] != parentDirName[:5]:
            log += u"Errore 01: Il codice del Tema del Metadato (%s) non corrisponde al nome della directory in cui è contenuto (%s)\n" %(metaFileName[:5],parentDirName[:5])
            errors.append("01")
            validMetafile = None

        #valutazione se metadato non compreso in SintesiClassiR
        if not (metaFileName.lower() in self.sintesiClassi_compare):
            log += u"Metadato non compreso nelle classi standard\n"
            if not tipoDato in ["5","6","7","8","9"]:
                log += u"Errore 02: Il codice (%s) che indica il tipo di dato deve essere nell'intervallo '5-9'\n" % tipoDato
                errors.append("02")
                validMetafile = None
        else:
            #metadato compreso in sintesi R
            if not tipoDato in ["0","1","2","3","4"]:
                log += u"Errore 03: Il codice (%s) che indica il tipo di dato deve essere nell'intervallo '0-4'\n" % tipoDato
                errors.append("03")
                validMetafile = None

        tipoErrore = None

        #valutazione dataset e codice
        if (tipoDato == "9" or tipoDato =="4") and tipoFormato != "dataset":
            tipoErrore = True

        #valutazione filesciolto e codice
        if (tipoDato == "5" or tipoDato =="0") and (tipoFormato == "dataset" or tipoFormato == "shp"):
            tipoErrore = True

        #valutazione shp punto e codice
        if (tipoDato == "3" or tipoDato =="8") and tipoFormato != "shp":
            tipoErrore = True

        if (tipoDato == "3" or tipoDato =="8") and tipoFormato == "shp" and tipoGeom != "punto":
            log += u"Errore 04: Il codice (%s) non corrisponde al tipo geometrico vettoriale (%s)\n" % (tipoDato,tipoGeom)
            errors.append("04")
            validMetafile = None

        #valutazione shp linea e codice
        if (tipoDato == "2" or tipoDato =="7") and tipoFormato != "shp":
            tipoErrore = True

        if (tipoDato == "2" or tipoDato =="7") and tipoFormato == "shp" and tipoGeom != "linea":
            log += u"Errore 04: Il codice (%s) non corrisponde al tipo geometrico vettoriale (%s)\n" % (tipoDato,tipoGeom)
            errors.append("04")
            validMetafile = None

        #valutazione shp area e codice
        if (tipoDato == "1" or tipoDato =="6") and tipoFormato != "shp":
            tipoErrore = True

        if (tipoDato == "1" or tipoDato =="6") and tipoFormato == "shp" and tipoGeom != "area":
            log += u"Errore 04: Il codice (%s) non corrisponde al tipo geometrico vettoriale (%s)\n" % (tipoDato,tipoGeom)
            errors.append("04")
            validMetafile = None

        if tipoErrore:
            log += u"Errore 05: Il codice (%s) che indica il tipo di dato non corrisponde al formato (%s)\n" % (tipoDato,tipoFormato)
            errors.append("05")
            validMetafile = None

        if tipoFormato != tipoFormatoDichiarato:
            log += u"Errore 06: Il formato del dato (%s) non corrisponde al formato dichiarato nei metadati (%s)\n" % (tipoFormato,tipoFormatoDichiarato)
            errors.append("06")

        if not self.validateTree(validateFile = metaFile, highlight = evidenzia):
            log += u"Errore 08: La compilazione dei metadati è incompleta\n"
            errors.append("08")
            validMetafile = None


        if metaFileName.lower() != idName.lower():
            log += u"Errore 07: Il nome del metadato (%s) differisce dal nome dell'identificatore (%s)\n" % (metaFileName,idName)
            errors.append("07")
            validMetafile = None

        if evidenzia:
            if "00" in errors or "01" in errors or "02" in errors or "03" in errors or "07" in errors:
                self.traverseTree(action = "highlight", keys = (pathNomeDato,pathIdDato))
            if "04" in errors:
                self.traverseTree(action = "highlight", keys = (pathGeomDato))
            if "05" in errors or "06" in errors:
                self.traverseTree(action = "highlight", keys = (pathFormatoDato))
        if log:
            self.widget.log.setPlainText(log)
        return (validMetafile,log, errors)


