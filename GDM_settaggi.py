# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GDM
                                 A QGIS plugin
 compilazione assistita metadati regione veneto
                              -------------------
        begin                : 2014-12-04
        git sha              : $Format:%H$
        copyright            : (C) 2014 by Enrico Ferreguti
        email                : enricofer@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtCore import *
from PyQt4.QtGui import *

import json
import os

class fileSelector(QObject):

    updated = pyqtSignal(str)

    def __init__(self, layoutContainer, label="Apri file", default='', fileType = '*.*'):
        super(fileSelector, self).__init__()
        self.fileName_widget = layoutContainer.itemAt(0).widget()
        self.searchFileButton = layoutContainer.itemAt(1).widget()
        self.label = label
        if default:
            self.default = default
            self.searchFileAction(default)
        else:
            self.default = self.fileName_widget.text()
        self.fileType = fileType
        self.fileName_widget.textChanged.connect(self.filenameChangedAction)
        self.searchFileButton.clicked.connect(self.searchFileAction)

    def searchFileAction(self,fileName = None):
        if not fileName:
            fileName = QFileDialog.getOpenFileName(None,self.label,self.default, self.fileType)
        if fileName:
            print "fileName", fileName
            self.fileName_widget.setText(fileName)

    def filenameChangedAction(self):
        if QFileInfo(self.fileName_widget.text()).exists():
            self.updated.emit(self.fileName_widget.text())


class settaggi:

    def __init__(self, parent):
        self.parent = parent
        self.dock = parent.dock
        self.dock.dbEntiTreeWidget.setColumnWidth(0,200)
        self.settaggiFileInfo = QFileInfo(os.path.join(os.path.dirname(__file__),"GDM_ini.json"))
        self.dock.aggiungiEnteButton.clicked.connect(self.aggiungiEnteAction)
        self.dock.eliminaEnteButton.clicked.connect(self.eliminaEnteButton)
        self.dock.dbEntiTreeWidget.itemChanged.connect(self.itemChangedAction)
        if not self.settaggiFileInfo.exists():
            self.setDefaults()
            self.scriviSettaggi()

        self.caricaSettaggi()

        if not os.path.exists(self.path_classi):
            self.setDefaults()
            self.scriviSettaggi()

        self.selettoreClassi = fileSelector(self.dock.sintesiClassi,label="Apri file di definizione classi", default=self.path_classi, fileType='*.csv')
        self.selettoreClassi.updated.connect(self.aggiornamentoClassiAction)

    def setDefaults(self):
        self.estensioni = "45.45420:11.97909:45.34250:11.80363"
        self.metadato = ''
        self.path_classi = os.path.join(os.path.dirname(__file__),"Defaults","SintesiClassi_R.csv")
        self.path_albero = ""
        self.enti = {
            "Comune di Padova, Settore urbanistica e servizi catastali": {
                "url": "http://www.padovanet.it",
                "telefono": "0498204638",
                "email": "urbanistica@comune.padova.it"
            },
            "Comune di Padova": {
                "url": "www.padovanet.it",
                "telefono": "049 8204638",
                "email": "urbanistica@comune.padova.it"
            },
            "Regione Veneto, UP per il SIT e la cartografia": {
                "url": "www.regione.veneto.it/Ambiente+e+Territorio/Territorio/Cartografia+Regionale",
                "telefono": "+39 0412792577",
                "email": ""
            },
            "Regione del Veneto - Unita di Progetto per il SIT e la Cartografia": {
                "url": "www.regione.veneto.it/Ambiente+e+Territorio/Territorio/Cartografia+Regionale",
                "telefono": "+39 0412792577",
                "email": ""
            },
            "Comune di Padova, Settore Servizi Informatici e Telematici, ufficio SIT": {
                "url": "www.padovanet.it",
                "telefono": "0498205300",
                "email": "sit@comune.padova.it"
            }
        }

    def aggiornamentoClassiAction(self,nuovo_classi_path):
        self.setPath_classi(nuovo_classi_path)

    def caricaSettaggi(self):
        if self.settaggiFileInfo.isReadable():
            with open(self.settaggiFileInfo.absoluteFilePath(),'rb') as settaggi_file:
                settaggi = json.load(settaggi_file)
                self.enti = settaggi['enti']
                self.metadato = settaggi['metadato']
                self.estensioni = settaggi['estensioni']
                self.path_albero = settaggi['path_albero']
                self.path_classi = settaggi['path_classi']
        else:
            self.enti = None
            self.estensioni = None
            self.classi = None
        for nome,recapito in self.enti.iteritems():
            self.aggiungiEnte(nome,recapito["url"],recapito["telefono"],recapito["email"])

    def setEstensioni(self,estensioni):
        self.estensioni = estensioni
        self.scriviSettaggi()

    def setPath_albero(self,path):
        self.path_albero = path
        self.scriviSettaggi()

    def setPath_classi(self,path):
        self.path_classi = path
        self.scriviSettaggi()

    def setMetadato(self,path):
        self.metadato = path

    def scriviSettaggi(self):
        settaggi = {
            "path_classi" : self.path_classi,
            "metadato": self.metadato,
            "path_albero" : self.path_albero,
            "estensioni" : self.estensioni,
            "enti" : self.enti
        }
        with open(self.settaggiFileInfo.absoluteFilePath(),'wb') as settaggi_file:
            json.dump(settaggi,settaggi_file)

    def aggiungiEnteAction(self):
        item = QTreeWidgetItem()
        #item.setText(0,"Ente:");
        item.setText(0,"Inserire nome ente");
        item.setFlags(Qt.ItemIsEditable|Qt.ItemIsEnabled)
        itemUrl = QTreeWidgetItem()
        itemUrl.setText(0,"Inserire indirizzo sito web");
        itemUrl.setFlags(Qt.ItemIsEditable|Qt.ItemIsEnabled)
        itemTel = QTreeWidgetItem()
        itemTel.setText(0,"Inserire numero di telefono");
        itemTel.setFlags(Qt.ItemIsEditable|Qt.ItemIsEnabled)
        itemEmail = QTreeWidgetItem()
        itemEmail.setText(0,"Inserire indirizzo email");
        itemEmail.setFlags(Qt.ItemIsEditable|Qt.ItemIsEnabled)
        item.addChildren([itemUrl,itemTel,itemEmail])
        self.dock.dbEntiTreeWidget.addTopLevelItem(item)
        self.dock.dbEntiTreeWidget.expandItem(item)

    def aggiungiEnte(self,nome,url,tel,email):
        item = QTreeWidgetItem()
        item.setText(0,nome);
        item.setFlags(Qt.ItemIsEditable|Qt.ItemIsEnabled)
        itemUrl = QTreeWidgetItem()
        itemUrl.setText(0,url);
        itemUrl.setFlags(Qt.ItemIsEditable|Qt.ItemIsEnabled)
        itemTel = QTreeWidgetItem()
        itemTel.setText(0,tel);
        itemTel.setFlags(Qt.ItemIsEditable|Qt.ItemIsEnabled)
        itemEmail = QTreeWidgetItem()
        itemEmail.setText(0,email);
        itemEmail.setFlags(Qt.ItemIsEditable|Qt.ItemIsEnabled)
        item.addChildren([itemUrl,itemTel,itemEmail])
        self.dock.dbEntiTreeWidget.addTopLevelItem(item)

    def eliminaEnteButton(self):
        #find parent child:
        currentItem = self.dock.dbEntiTreeWidget.currentItem()
        if currentItem.parent():
            removeItem = currentItem.parent()
        else:
            removeItem = currentItem
        root = self.dock.dbEntiTreeWidget.invisibleRootItem()
        root.removeChild(removeItem)
        self.updateDb()

    def itemChangedAction (self,item,col):
        self.updateDb()

    def updateDb(self):
        root = self.dock.dbEntiTreeWidget.invisibleRootItem()
        self.enti = {}
        for itemId in range(0,root.childCount()):
            item = root.child(itemId)
            self.enti[item.text(0)]={"url":item.child(0).text(0),"telefono":item.child(1).text(0),"email":item.child(2).text(0)}
        self.scriviSettaggi()
        self.parent.metaDialogTab.setResponsibleParties(self.enti)

