# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GDM - class GDM_albero
                                 A QGIS plugin
 compilazione assistita metadati regione veneto
                              -------------------
        begin                : 2014-12-04
        git sha              : $Format:%H$
        copyright            : (C) 2014 by Enrico Ferreguti
        email                : enricofer@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

#from PyQt4 import QtCore, QtGui

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from ui_mkdir_dialog import Ui_mkdirDialog

import os
# create the dialog for zoom to point


class mkdirDialog(QDialog, Ui_mkdirDialog):
    def __init__(self):
        QDialog.__init__(self)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)
        self.buttonBox.accepted.connect(self.mkdir)
        self.buttonBox.rejected.connect(self.cancel)

    def cancel(self):
        #self.hide()
        pass

    def getDirName(self,basePath):
        self.basePath = basePath
        self.show()

    def mkdir(self):
        newName = self.sourceString.text()
        baseDir = QDir(self.basePath)
        if not baseDir.mkdir(newName):
            QMessageBox.critical(None, "Impossibile creare la nuova directory %s" % newName)
