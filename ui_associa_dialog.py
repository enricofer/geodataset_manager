# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_associa_dialog.ui'
#
# Created: Mon Feb 09 13:29:38 2015
#      by: PyQt4 UI code generator 4.10.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_associaDialog(object):
    def setupUi(self, associaDialog):
        associaDialog.setObjectName(_fromUtf8("associaDialog"))
        associaDialog.resize(411, 207)
        self.verticalLayout_6 = QtGui.QVBoxLayout(associaDialog)
        self.verticalLayout_6.setObjectName(_fromUtf8("verticalLayout_6"))
        self.label_2 = QtGui.QLabel(associaDialog)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout_6.addWidget(self.label_2)
        self.classiPrevisteCombo = QtGui.QComboBox(associaDialog)
        self.classiPrevisteCombo.setObjectName(_fromUtf8("classiPrevisteCombo"))
        self.verticalLayout_6.addWidget(self.classiPrevisteCombo)
        self.verticalLayout_5 = QtGui.QVBoxLayout()
        self.verticalLayout_5.setSpacing(2)
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.classeAggiuntaCheck = QtGui.QCheckBox(associaDialog)
        self.classeAggiuntaCheck.setObjectName(_fromUtf8("classeAggiuntaCheck"))
        self.verticalLayout_5.addWidget(self.classeAggiuntaCheck)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setSpacing(2)
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.verticalLayout_3 = QtGui.QVBoxLayout()
        self.verticalLayout_3.setSpacing(3)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.codiceTemaLabel = QtGui.QLabel(associaDialog)
        self.codiceTemaLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.codiceTemaLabel.setObjectName(_fromUtf8("codiceTemaLabel"))
        self.verticalLayout_3.addWidget(self.codiceTemaLabel)
        self.codiceTemaEdit = QtGui.QLineEdit(associaDialog)
        self.codiceTemaEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.codiceTemaEdit.setReadOnly(True)
        self.codiceTemaEdit.setObjectName(_fromUtf8("codiceTemaEdit"))
        self.verticalLayout_3.addWidget(self.codiceTemaEdit)
        self.horizontalLayout_3.addLayout(self.verticalLayout_3)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setSpacing(3)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.codiceClasseLabel = QtGui.QLabel(associaDialog)
        self.codiceClasseLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.codiceClasseLabel.setObjectName(_fromUtf8("codiceClasseLabel"))
        self.verticalLayout.addWidget(self.codiceClasseLabel)
        self.codiceClasseEdit = QtGui.QLineEdit(associaDialog)
        self.codiceClasseEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.codiceClasseEdit.setObjectName(_fromUtf8("codiceClasseEdit"))
        self.verticalLayout.addWidget(self.codiceClasseEdit)
        self.horizontalLayout_3.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setSpacing(3)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.codiceGeomLabel = QtGui.QLabel(associaDialog)
        self.codiceGeomLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.codiceGeomLabel.setObjectName(_fromUtf8("codiceGeomLabel"))
        self.verticalLayout_2.addWidget(self.codiceGeomLabel)
        self.codiceGeomEdit = QtGui.QLineEdit(associaDialog)
        self.codiceGeomEdit.setMaximumSize(QtCore.QSize(60, 16777215))
        self.codiceGeomEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.codiceGeomEdit.setReadOnly(True)
        self.codiceGeomEdit.setObjectName(_fromUtf8("codiceGeomEdit"))
        self.verticalLayout_2.addWidget(self.codiceGeomEdit)
        self.horizontalLayout_3.addLayout(self.verticalLayout_2)
        self.verticalLayout_4 = QtGui.QVBoxLayout()
        self.verticalLayout_4.setSpacing(3)
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.descrizioneClasseLabel = QtGui.QLabel(associaDialog)
        self.descrizioneClasseLabel.setAlignment(QtCore.Qt.AlignCenter)
        self.descrizioneClasseLabel.setObjectName(_fromUtf8("descrizioneClasseLabel"))
        self.verticalLayout_4.addWidget(self.descrizioneClasseLabel)
        self.descrizioneClasseEdit = QtGui.QLineEdit(associaDialog)
        self.descrizioneClasseEdit.setMinimumSize(QtCore.QSize(200, 0))
        self.descrizioneClasseEdit.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.descrizioneClasseEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.descrizioneClasseEdit.setObjectName(_fromUtf8("descrizioneClasseEdit"))
        self.verticalLayout_4.addWidget(self.descrizioneClasseEdit)
        self.horizontalLayout_3.addLayout(self.verticalLayout_4)
        self.verticalLayout_5.addLayout(self.horizontalLayout_3)
        self.label_8 = QtGui.QLabel(associaDialog)
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.verticalLayout_5.addWidget(self.label_8)
        self.nomeSHPLabel = QtGui.QLabel(associaDialog)
        self.nomeSHPLabel.setObjectName(_fromUtf8("nomeSHPLabel"))
        self.verticalLayout_5.addWidget(self.nomeSHPLabel)
        self.esitoLabel = QtGui.QLabel(associaDialog)
        self.esitoLabel.setText(_fromUtf8(""))
        self.esitoLabel.setObjectName(_fromUtf8("esitoLabel"))
        self.verticalLayout_5.addWidget(self.esitoLabel)
        self.verticalLayout_6.addLayout(self.verticalLayout_5)
        self.buttonBox = QtGui.QDialogButtonBox(associaDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName(_fromUtf8("buttonBox"))
        self.verticalLayout_6.addWidget(self.buttonBox)
        spacerItem = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_6.addItem(spacerItem)

        self.retranslateUi(associaDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("accepted()")), associaDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL(_fromUtf8("rejected()")), associaDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(associaDialog)

    def retranslateUi(self, associaDialog):
        associaDialog.setWindowTitle(_translate("associaDialog", "Dialog", None))
        self.label_2.setText(_translate("associaDialog", "Classi previste per il Tema selezionato:", None))
        self.classeAggiuntaCheck.setText(_translate("associaDialog", "Classe aggiunta", None))
        self.codiceTemaLabel.setText(_translate("associaDialog", "Codice Tema", None))
        self.codiceClasseLabel.setText(_translate("associaDialog", "Codice Classe", None))
        self.codiceGeomLabel.setText(_translate("associaDialog", "Geometria", None))
        self.descrizioneClasseLabel.setText(_translate("associaDialog", "Descrizione Classe", None))
        self.label_8.setText(_translate("associaDialog", "Nome SHP: ", None))
        self.nomeSHPLabel.setText(_translate("associaDialog", "TextLabel", None))

