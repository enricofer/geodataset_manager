# -*- coding: utf-8 -*-
"""
/***************************************************************************
 GDM - class GDM_albero
                                 A QGIS plugin
 compilazione assistita metadati regione veneto
                              -------------------
        begin                : 2014-12-04
        git sha              : $Format:%H$
        copyright            : (C) 2014 by Enrico Ferreguti
        email                : enricofer@gmail.com
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtXml import *
from qgis.core import *
from qgis.utils import *
from qgis.gui import *

import subprocess, os
import sys, csv, codecs, cStringIO
import shutil
import json

from GDM_rename_dialog import renameDialog
from GDM_mkdir_dialog import mkdirDialog
from GDM_associa_dialog import associaDialog

count = 0

def hasXml(file):
    if QFileInfo(os.path.join(file.path(),file.baseName()+".xml")).exists():
        return True
    else:
        return None

class UTF8Recoder:
    """
    Iterator that reads an encoded stream and reencodes the input to UTF-8
    """
    def __init__(self, f, encoding):
        self.reader = codecs.getreader(encoding)(f)

    def __iter__(self):
        return self

    def next(self):
        return self.reader.next().encode("utf-8")

class UnicodeReader:
    """
    A CSV reader which will iterate over lines in the CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        f = UTF8Recoder(f, encoding)
        self.reader = csv.reader(f, dialect=dialect, **kwds)

    def next(self):
        row = self.reader.next()
        return [unicode(s, "utf-8") for s in row]

    def __iter__(self):
        return self

class UnicodeWriter:
    """
    A CSV writer which will write rows to CSV file "f",
    which is encoded in the given encoding.
    """

    def __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
        # Redirect output to a queue
        self.queue = cStringIO.StringIO()
        self.writer = csv.writer(self.queue, dialect=dialect, **kwds)
        self.stream = f
        self.encoder = codecs.getincrementalencoder(encoding)()

    def writerow(self, row):
        self.writer.writerow([s.encode("utf-8") for s in row])
        # Fetch UTF-8 output from the queue ...
        data = self.queue.getvalue()
        data = data.decode("utf-8")
        # ... and reencode it into the target encoding
        data = self.encoder.encode(data)
        # write to the target stream
        self.stream.write(data)
        # empty queue
        self.queue.truncate(0)

    def writerows(self, rows):
        for row in rows:
            self.writerow(row)


class MyDelegate(QItemDelegate):
    def __init__(self, parent, parentClass):
        QItemDelegate.__init__(self, parent)
        self.parentClass = parentClass
        self.FStree = parentClass.widget.albero
        self.FSmodel = parentClass.FSmodel
        self.metadataTool = parentClass.metadataTool

    def paint(self, painter, option, index):
        #global count
        painter.save()
        currentFile = self.FSmodel.fileInfo(index)
        currentMetadata = QFileInfo(os.path.join(currentFile.absolutePath(),currentFile.baseName()+".xml"))
        #print currentFile.completeBaseName(),currentMetadata.completeBaseName()
        #count += 1
        #print count
        #if currentFile.isDir():
        #    pass
        #else:
        if not currentMetadata.exists():
            painter.setPen(QPen(Qt.darkGray))
        else:
            validMetadata,log,errors = self.metadataTool.validaFileMetadato(currentMetadata.filePath(),log = None)
            if errors == []:
                painter.setPen(QPen(Qt.darkGreen))
            else:
                painter.setPen(QPen(Qt.darkRed))
            if not self.metadataTool.validateMetadataName(currentMetadata.completeBaseName()):
                currentFont = painter.font()
                currentFont.setItalic(True)
                painter.setFont(currentFont)

        #if currentFile.exists()
        #    if not self.metadataTool.validateTree(currentFile):
        #        painter.fillRect(option.rect,QBrush(QColor( 253, 89, 95 )))
        #if painter.pen().color() != Qt.darkGreen:
        #    parentIndex = index.parent()
        #    while parentIndex.isValid():
        #        cellWidget = self.FStree.indexWidget(parentIndex)
        #        cellWidget.setStyle(QStyle)
        #        parentIndex = parentIndex.parent()

        painter.drawText(option.rect, Qt.AlignLeft, currentFile.fileName())
        #print currentFile.path()
        painter.restore()

class repertorio:
    def __init__(self, parent):
        self.widget = parent.dock
        self.settaggi = parent.gestioneSettaggi
        self.iface = parent.iface
        self.metadataTool = parent.metaDialogTab
        self.plugin_dir = os.path.dirname(__file__)
        self.widget.openButton.clicked.connect(self.openf)
        self.widget.saveButton.clicked.connect(self.sintesi)
        self.widget.collFileTreeButton.clicked.connect(self.collFileTreeFunc)
        self.widget.expFileTreeButton.clicked.connect(self.expFileTreeFunc)
        self.widget.lineEdit.setText('Seleziona un geodataset da organizzare')
        #self.widget.scan.clicked.connect(self.scan)
        self.s = QSettings(os.path.join(self.plugin_dir,".GDM"),QSettings.IniFormat)
        self.widget.albero.clicked.connect(self.clickAction)
        self.widget.albero.doubleClicked.connect(self.doubleClickAction)
        self.FSmodel = QFileSystemModel()
        #self.oldDir = self.s.value("GDM/fileState","")
        self.oldDir = self.settaggi.path_albero
        if self.oldDir and QDir(self.oldDir).isReadable():
            self.widget.lineEdit.setText(self.oldDir)
            self.scan()
        self.renameDialog = renameDialog(parent)
        self.mkdirDialog = mkdirDialog()
        self.clipboardInstance = QApplication.clipboard()
        self.clipboard = {"mode":"","payload":""}
        self.widget.progressBar.reset()
        self.widget.progressBar.hide()
        self.associaTool = associaDialog(self)

    def openf(self):
        fileName = QFileDialog.getExistingDirectory(None,"Apri cartella geodataset", self.oldDir)
        if fileName:
            self.widget.lineEdit.setText(fileName)
            #self.s.setValue("GDM/fileState",fileName)
            self.settaggi.setPath_albero(fileName)
            self.scan()

    def scan(self):
        self.FSmodel.setRootPath(QDir(self.widget.lineEdit.text()).absolutePath())
        self.FSmodel.setNameFilters(["*.pdf","*.shp","*.doc","*.jpg","*.tif","*.xls","*.png","*.txt","*.zip"])
        self.FSmodel.setFilter(QDir.Files | QDir.AllDirs | QDir.NoDotAndDotDot)
        self.FSmodel.setNameFilterDisables(False)
        #FSmodel.setReadOnly(False)

        #self.widget.albero.model.setFilter()
        #self.widget.albero.model.setRootPath(QDir(self.widget.lineEdit.text()).path());
        #rootPathModel = QSortFilterProxyModel()
        #rootPathModel.setSourceModel(self.widget.albero.model)
        #rootPathModel.setFilterWildcard("C:/tmp/c1103075_PTCP/c1103075_PTCP*")
        #print rootPathModel.filterRegExp()
        #self.widget.albero.setFilter()
        #print self.widget.albero.model.rootPath()
        self.widget.albero.setModel(self.FSmodel)
        customItemDelegate = MyDelegate(self.widget.albero,self)
        self.widget.albero.setItemDelegate(customItemDelegate)
        idx = self.FSmodel.index(QDir(self.widget.lineEdit.text()).path())
        self.widget.albero.setRootIndex(idx)
        self.widget.albero.hideColumn(1)
        self.widget.albero.hideColumn(2)
        self.widget.albero.hideColumn(3)

    def newDirFunc(self):
        self.mkdirDialog.getDirName(self.currentFile.filePath())
        self.widget.albero.expand(self.FSmodel.index(self.currentFile.filePath()))

    def collFileTreeFunc(self):
        self.widget.albero.collapseAll()
        self.widget.albero.expand(self.FSmodel.index(self.currentFile.filePath()))

    def expFileTreeFunc(self):
        self.widget.albero.expandAll()

    def openFunc(self):
        #print self.currentFile.suffix()
        #print self.currentFile.filePath()
        if self.currentFile.suffix() == 'shp':
            vlayer = QgsVectorLayer(self.currentFile.filePath(), self.currentFile.baseName(), "ogr")
            QgsMapLayerRegistry.instance().addMapLayer(vlayer)
            self.iface.mapCanvas().setExtent(vlayer.extent())
        else:
            if sys.platform.startswith('darwin'):
                subprocess.call(('open', self.currentFile.filePath()))
            elif os.name == 'nt':
                os.startfile(self.currentFile.filePath())
            elif os.name == 'posix':
                subprocess.call(('xdg-open', self.currentFile.filePath()))

    def openXmlFunc(self):
        self.metadataTool.openMetadata(self.currentMetadata.filePath())
        self.metadataTool.validaFileMetadato(self.currentMetadata.filePath(),evidenzia=True)
        self.widget.tabWidget.setCurrentIndex(2)
        self.metadataTool.expandAction()
        #self.metadataTool.openMetadata(QFileInfo(os.path.join(self.currentFile.path(),self.currentFile.completeBaseName()+'.xml')).filePath())
        #self.widget.XMLFilename.setText(self.currentMetadata.filePath())
        #XMLFile = open(self.currentMetadata.filePath(), "r")
        #XMLdef = QDomDocument("")
        #XMLdef.setContent(XMLFile.read())
        #self.widget.control.setText(XMLdef.toString(2))
        #self.widget.XMLFilename.setText(self.currentMetadata.filePath())


    def newXmlFunc(self):
        self.metadataTool.newMetadata(fileName=self.currentMetadata.filePath())
        self.widget.tabWidget.setCurrentIndex(2)
        self.metadataTool.expandAction()

    def validFunc(self):
        print self.metadataTool.validateTree(self.currentMetadata.filePath())
        print self.metadataTool.validateTree(QFileInfo(os.path.join(self.currentFile.path(),self.currentFile.completeBaseName()+'.xml')).filePath())


    def renameFunc(self):
        self.renameDialog.ren(self.currentFile.filePath())

    def cutFunc(self):
        self.clipboard["mode"]="cut"
        self.clipboard['payload']= self.currentFile.filePath()

    def copyFunc(self):
        self.clipboard["mode"]="copy"
        self.clipboard['payload']= self.currentFile.filePath()

    def cutPathFunc(self):
        pass

    def pasteFunc(self):
        if self.currentFile.isDir():
            dest = self.currentFile.filePath()
        else:
            dest = self.currentFile.absolutePath()
        if self.clipboard["mode"] == "copy":
            self.copyFile(self.clipboard["payload"],dest)
            self.clipboard = {"mode":"","payload":""}
        if self.clipboard["mode"] == "cut":
            self.moveFile(self.clipboard["payload"],dest)
            #self.deleteFile(self.clipboard["payload"])
            self.clipboard = {"mode":"","payload":""}



    def delFunc(self):
        reply = QMessageBox.question(None,"Conferma Cancellazione","Vuoi veramente cancellare il file %s ?" % self.currentFile.fileName(), QMessageBox.Yes, QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.deleteFile(self.currentFile.filePath())


    def contextMenuRequest(self,index):
        self.currentFile = self.FSmodel.fileInfo(index)
        currentDir = self.currentFile.absoluteDir()
        self.currentMetadata = QFileInfo(os.path.join(self.currentFile.path(),self.currentFile.completeBaseName()+'.xml'))
        nameValidator = QRegExp("^[abc][0-9]{4}[_][A-Za-z0-9]{1,21}$")
        #print self.currentFile.path()
        #print self.currentMetadata.filePath()
        contextMenu = QMenu()
        if self.currentFile.isDir():
            self.newdirAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","open.png")),"Nuova Directory")
            self.newdirAction.triggered.connect(self.newDirFunc)
            if self.iface.legendInterface().currentLayer() and nameValidator.exactMatch(self.currentFile.completeBaseName()):
                self.importaLayerAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","applyTemplate.png")),"Importa nella collezione il Layer %s " % self.iface.legendInterface().currentLayer().name())
                self.importaLayerAction.triggered.connect(self.importaLayerFunc)
                self.importaFileAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","applyTemplate.png")),"Importa file nella collezione")
                self.importaFileAction.triggered.connect(self.importaFileFunc)
            self.updateMetadataAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","saveTemplate.png")),"Aggiorna Metadati a RNDT2011")
            self.updateMetadataAction.triggered.connect(self.updateMetadataFunc)
            self.applyTemplateAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","applyTemplate.png")),"Applica Template ai Metadati")
            self.applyTemplateAction.triggered.connect(self.applyTemplateFunc)
            self.openFolderAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","open.png")),"Apri folder " + self.currentFile.fileName())
            self.openFolderAction.triggered.connect(lambda: self.openFolderFunc(self.currentFile.absolutePath()))
        else:
            self.openAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","open.png")),"Apri " + self.currentFile.fileName())
            self.openAction.triggered.connect(self.openFunc)
            self.openFolderAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","open.png")),"Apri folder " + self.currentFile.absoluteDir().dirName())
            self.openFolderAction.triggered.connect(lambda: self.openFolderFunc(self.currentFile.absoluteDir().absolutePath()))
        if self.currentMetadata.exists():
            self.openXmlAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","openxml.png")),"Apri Metadato")
            self.openXmlAction.triggered.connect(self.openXmlFunc)
            validMetadata,log,errors = self.metadataTool.validaFileMetadato(self.currentMetadata.filePath())
        self.newXmlAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","newxml.png")),"Genera Metadato")
        self.newXmlAction.triggered.connect(self.newXmlFunc)
        #self.validAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","valid.png")),"Valida")
        #self.validAction.triggered.connect(self.validFunc)
        self.renameAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","rename.png")),"Rinomina")
        self.renameAction.triggered.connect(self.renameFunc)
        #if self.currentFile.isFile():
        self.cutAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","cut.png")),"Taglia")
        self.cutAction.triggered.connect(self.cutFunc)
        self.copyAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","copy.png")),"Copia")
        self.copyAction.triggered.connect(self.copyFunc)
        if self.clipboard["mode"] != "":
            self.pasteAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","paste.png")),"Incolla in " + self.currentFile.absoluteDir().dirName())
            self.pasteAction.triggered.connect(self.pasteFunc)
        self.delAction = contextMenu.addAction(QIcon(os.path.join(self.plugin_dir,"icons","del.png")),"Cancella")
        self.delAction.triggered.connect(self.delFunc)
        if self.currentMetadata.exists() and not validMetadata:
            contextMenu.addSeparator()
            self.pasteAction = contextMenu.addAction("Errori rilevati:")
            errs = log.split("\n")
            for err in errs:
                self.pasteAction = contextMenu.addAction(err)
        contextMenu.exec_(QCursor.pos())

    def doubleClickAction(self,index):
        self.currentFile = self.FSmodel.fileInfo(index)
        if self.currentFile.isFile():
            self.openFunc()


    def clickAction(self,index):
        self.contextMenuRequest(index)

    def openFolderFunc(self, d):
        print d
        if not d:
            d = self.currentFile.absolutePath()
        print d
        if sys.platform=='win32':
            subprocess.Popen(['start', d], shell= True)

        elif sys.platform=='darwin':
            subprocess.Popen(['open', d])

        else:
            try:
                subprocess.Popen(['xdg-open', d])
            except OSError:
                pass
                # er, think of something else to try
                # xdg-open *should* be supported by recent Gnome, KDE, Xfce

    def deleteFile(self,fileName):
        iter = self.getAllFileWithSameName(fileName)
        print iter
        base = QFileInfo(fileName)
        if base.isDir():
            baseDir = QDir(base.filePath())
            if len (baseDir.entryInfoList(QDir.NoDotAndDotDot|QDir.AllEntries)) == 0:
                os.rmdir(baseDir.absolutePath())
            else:
                QMessageBox.critical(None, u"Impossibile Cancellare la directory", u"La directory %s non è vuota, impossibile la cancellazione" % base.fileName())
        else:
            #QMessageBox.critical(None, "elenco dei files da cancellare", "".join(iter))
            for iterFile in iter:
                file = QFile(os.path.join(base.absolutePath(),iterFile))
                if not file.remove():
                    QMessageBox.critical(None, "Impossibile Cancellare il file", "Impossibile cancellare il file:\n %s" % file.fileName())
                else:
                    print "delete", iterFile

    def copyFile(self,sourceName,targetDir):
        iter = self.getAllFileWithSameName(sourceName)
        print sourceName, iter
        base = QFileInfo(sourceName)
        #QMessageBox.critical(None, "elenco dei files da copiare", "".join(iter))
        for iterFile in iter:
            try:
                shutil.copy(os.path.join(os.path.dirname(sourceName), iterFile),targetDir)
            except Exception as e:
                QMessageBox.critical(None, "Impossibile Copiare il file", "Impossibile copiare il file %s:\n in %s\n%s" % (iterFile,targetDir,e))

    def moveFile(self,sourceName,targetDir):
        iter = self.getAllFileWithSameName(sourceName)
        print sourceName, iter
        base = QFileInfo(sourceName)
        #QMessageBox.critical(None, "elenco dei files da copiare", "".join(iter))
        for iterFile in iter:
            try:
                shutil.move(os.path.join(os.path.dirname(sourceName), iterFile),targetDir)
            except Exception as e:
                QMessageBox.critical(None, "Impossibile Spostare il file", "Impossibile copiare il file %s:\n in %s\n%s" % (iterFile,targetDir,e))


    def getAllFileWithSameName(self,fileName):
        filebasename, file_extension = os.path.splitext(fileName)
        fileInfo = QFileInfo(fileName)
        if file_extension == '.shp':
            fileDir = fileInfo.absoluteDir()
            return fileDir.entryList([fileInfo.baseName()+"*"])
        else:
            entrylist = [fileName]
            metadataFilePath = os.path.join(filebasename + '.xml')
            print "metadataFilePath", metadataFilePath, os.path.exists(metadataFilePath)
            if os.path.exists(metadataFilePath):
                entrylist.append(metadataFilePath)
            return entrylist

    def sintesi(self):
        fileCSVName = QFileDialog().getSaveFileName (None,"Salva tabella di sintesi", os.path.join(self.FSmodel.rootPath(),'SintesiClassi_C.csv'), "*.csv")
        if not fileCSVName:
            return
        self.metadataDb = self.metadataTool.sintesiClassiR
        self.pathFormatoDato = "//distributionInfo//MD_Distribution//distributionFormat//MD_Format//name//gco:CharacterString"
        self.pathGeomDato = "//spatialRepresentationInfo//MD_VectorSpatialRepresentation//geometricObjects//MD_GeometricObjects//geometricObjectType//MD_GeometricObjectTypeCode"
        self.pathNomeDato = "//fileIdentifier//gco:CharacterString"
        self.pathIdDato = "//identificationInfo//MD_DataIdentification//citation//CI_Citation//identifier//MD_Identifier//code//gco:CharacterString"
        self.pathDescrizione = "//identificationInfo//MD_DataIdentification//abstract//gco:CharacterString"
        self.pathEnte = "//identificationInfo//MD_DataIdentification//citation//CI_Citation//citedResponsibleParty//CI_ResponsibleParty//organizationName//gco:CharacterString"
        self.pathIdPrecedente = "//gmd:identificationInfo//gmd:MD_DataIdentification//gmd:citation//gmd:CI_Citation//gmd:otherCitationDetails//gco:CharacterString"

        with open(os.path.join(os.path.dirname(__file__), 'temi.json')) as json_file:  
            temi = json.load(json_file)

        #print "metadataDb", self.metadataDb.keys()
        #self.recurseFileTree(self.FSmodel.index(self.FSmodel.rootPath()))
        for root, dirs, files in os.walk(self.FSmodel.rootPath()):
            for name in files:
                currentFile = QFileInfo(os.path.join(root, name))
                if currentFile.suffix()=="xml":
                    currentMetadata = QFileInfo(os.path.join(currentFile.path(),currentFile.completeBaseName()+'.xml'))
                    validMetadata,log,errors = self.metadataTool.validaFileMetadato(currentMetadata.filePath())
                    #print "Metadata", currentMetadata,errors
                    if "00" in errors:
                        continue
                    tipo = self.metadataTool.checkDataFormat(currentMetadata.filePath())
                    if tipo == "dataset":
                        tipoTab = "D"
                    elif tipo == "shp":
                        tipoTab = "F"
                    elif tipo == "xls":
                        tipoTab = "I"
                    else:
                        tipoTab = "A"
                    metadataContent = self.metadataTool.XMLInspection(currentMetadata.filePath(),(self.pathNomeDato,self.pathDescrizione,self.pathEnte,self.pathIdPrecedente))
                    print "metadataContent", metadataContent[0], ">",metadataContent[3],"<"
                    
                    if metadataContent[0] in self.metadataDb:
                        riferimento = ''
                    else:
                        riferimento = "RIFERIMENTO A CLASSE SOSTITUITA"

                    try:
                        tema = temi[metadataContent[0][:5]]
                    except:
                        tema = {"Gruppo":"-","Matrice":"-","Temi":"-"}
                    metaInfo = [metadataContent[0] or '',tema["Gruppo"],tema["Matrice"],tema["Temi"],metadataContent[1] or '',tipoTab,metadataContent[2] or '', "SI",metadataContent[3] or riferimento,"SI"] 
                    if validMetadata:
                        metaInfo.append('SI')
                    else:
                        metaInfo.append('NO')
                    
                    self.metadataDb[metadataContent[0]] = metaInfo
        #print self.metadataDb
        csvFile = open(fileCSVName, 'wb')
        csvWriter = UnicodeWriter(csvFile, delimiter='\t', quotechar='"')
        csvWriter.writerow(["Nome","Gruppo","Matrice","Temi","Descrizione","Tipo di dato","Fonte","Presente SI/NO","Presente in altra cartella","Utilizzato SI/NO","Valido SI/NO"])
        
        #correzione per classe non utilizzata
        for key, item in self.metadataDb.iteritems():    
            if len(item) == 10:
                item[7] = 'NO'
                item[8] = 'Non utilizzata'

        #correzione di presente in altra classe
        for key, item in self.metadataDb.iteritems():
            print key,len(item)
            if len(item) == 11:
                if item[8]:
                    print "RIFERIMENTO %s A (%s)" % (item[0],item[8])
                    item[9] = 'SI'
                    if item[8] in self.metadataDb and item[8] != item[0]:
                        rif_item = self.metadataDb[item[8]]
                        if rif_item[9]=='NO':
                            rif_item[7] = 'SI'
                            rif_item[8] = item[0]
                            item[8] = ""
                    else:
                        item[8] = "VERIFICARE: " + item[8]

        for item in sorted(self.metadataDb.keys()):
            csvWriter.writerow(self.metadataDb[item])
        #for metadataName,metadataRow  in self.metadataDb.iteritems():
        #    csvWriter.writerow(metadataRow)
        csvFile.close()

    def updateMetadataFunc(self):
        count = 0
        self.widget.progressBar.show()
        for root, dirs, files in os.walk(self.currentFile.filePath()):
            for name in files:
                currentFile = QFileInfo(os.path.join(root, name))
                if currentFile.suffix()=="xml":
                    count += 1
        self.widget.progressBar.setRange(0,count)
        count = 0
        for root, dirs, files in os.walk(self.currentFile.filePath()):
            for name in files:
                currentFile = QFileInfo(os.path.join(root, name))
                if currentFile.suffix()=="xml":
                    self.metadataTool.openMetadata(currentFile.absoluteFilePath())
                    self.metadataTool.saveMetadata(currentFile.absoluteFilePath())
                    count += 1
                    self.widget.progressBar.setValue(count)
        self.widget.progressBar.hide()


    def applyTemplateFunc(self):
        fileName = QFileDialog.getOpenFileName (None,"Inserisci modello di metadato xml da applicare alla sottodirectory", os.path.join(os.path.dirname(__file__),"Templates"), "*.xml")
        if fileName:
            count = 0
            self.widget.progressBar.show()
            for root, dirs, files in os.walk(self.currentFile.filePath()):
                for name in files:
                    currentFile = QFileInfo(os.path.join(root, name))
                    if currentFile.suffix()=="xml":
                        count += 1
            self.widget.progressBar.setRange(0,count)
            count = 0
            XMLFile = open(fileName, "r")
            self.metadataTool.XMLTemplate = QDomDocument("")
            self.metadataTool.XMLTemplate.setContent(XMLFile.read())
            for root, dirs, files in os.walk(self.currentFile.filePath()):
                for name in files:
                    currentFile = QFileInfo(os.path.join(root, name))
                    if currentFile.suffix()=="xml":
                        self.metadataTool.openMetadata(currentFile.absoluteFilePath())
                        self.metadataTool.traverseTree(action="loadTemplate", overwrite = True)
                        self.metadataTool.saveMetadata(currentFile.absoluteFilePath())
                        count += 1
                        self.widget.progressBar.setValue(count)
            self.widget.progressBar.hide()
            XMLFile.close()

    def importaLayerFunc(self):
        self.associaTool.associaATema(self.iface.legendInterface().currentLayer(),self.currentFile.filePath())

    def importaFileFunc(self):
        self.associaTool.associaATema(None,self.currentFile.filePath())
